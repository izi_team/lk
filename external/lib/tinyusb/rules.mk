LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

GLOBAL_INCLUDES += \
	$(LOCAL_DIR)/src \
	$(LOCAL_DIR)/src/device \
	$(LOCAL_DIR)/src/class/cdc

MODULE_SRCS += \
    $(LOCAL_DIR)/src/tusb.c \
	$(LOCAL_DIR)/src/device/usbd.c \
	$(LOCAL_DIR)/src/device/usbd_control.c \
	$(LOCAL_DIR)/src/common/tusb_fifo.c \
	$(LOCAL_DIR)/src/portable/raspberrypi/rp2040/dcd_rp2040.c \
	$(LOCAL_DIR)/src/portable/raspberrypi/rp2040/rp2040_usb.c \
	$(LOCAL_DIR)/src/class/cdc/cdc_device.c

include make/module.mk
