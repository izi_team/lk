LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

GLOBAL_INCLUDES += \
	$(LOCAL_DIR)/hardware/regs \
	$(LOCAL_DIR)/pico_platform/include \
	$(LOCAL_DIR)/hardware_base/include \
	$(LOCAL_DIR)/hardware_clocks/include \
	$(LOCAL_DIR)/hardware_gpio/include \
	$(LOCAL_DIR)/hardware_irq/include \
	$(LOCAL_DIR)/hardware_pll/include \
	$(LOCAL_DIR)/hardware_pwm/include \
	$(LOCAL_DIR)/hardware_adc/include \
	$(LOCAL_DIR)/hardware_spi/include \
	$(LOCAL_DIR)/hardware_sync/include \
	$(LOCAL_DIR)/hardware_resets/include \
	$(LOCAL_DIR)/hardware_timer/include \
	$(LOCAL_DIR)/hardware_uart/include \
	$(LOCAL_DIR)/hardware_watchdog/include \
	$(LOCAL_DIR)/hardware_xosc/include \
	$(LOCAL_DIR)/hardware_irq/include \
	$(LOCAL_DIR)/hardware_claim/include \
	$(LOCAL_DIR)/boot_stage2/asminclude \
	$(LOCAL_DIR)/hardware_i2c/include

MODULE_SRCS += \
	$(LOCAL_DIR)/boot_stage2/boot2_w25q080.S \
	$(LOCAL_DIR)/pico_standard_link/crt0.S \
	$(LOCAL_DIR)/hardware_clocks/clocks.c \
	$(LOCAL_DIR)/hardware_gpio/gpio.c \
	$(LOCAL_DIR)/hardware_pll/pll.c \
	$(LOCAL_DIR)/hardware_uart/uart.c \
	$(LOCAL_DIR)/hardware_watchdog/watchdog.c \
	$(LOCAL_DIR)/hardware_xosc/xosc.c \
	$(LOCAL_DIR)/hardware_irq/irq.c \
	$(LOCAL_DIR)/hardware_irq/irq_handler_chain.S \
	$(LOCAL_DIR)/hardware_sync/sync.c \
	$(LOCAL_DIR)/hardware_claim/claim.c \
	$(LOCAL_DIR)/hardware_adc/adc.c \
	$(LOCAL_DIR)/hardware_i2c/i2c.c

include make/module.mk