LOCAL_DIR := $(GET_LOCAL_DIR)

GLOBAL_INCLUDES += \
	$(LOCAL_DIR)/pico_base/include \
	$(LOCAL_DIR)/pico_binary_info/include \
	$(LOCAL_DIR)/pico_time/include
#	$(LOCAL_DIR)/pico_sync/include \


# MODULE_SRCS += \
# 	$(LOCAL_DIR)/pico_time/time.c
#	$(LOCAL_DIR)/pico_sync/mutex.c \
#	$(LOCAL_DIR)/pico_time/timeout_helper.c
#	$(LOCAL_DIR)/pico_sync/sem.c \
#	$(LOCAL_DIR)/pico_sync/lock_core.c \
#	$(LOCAL_DIR)/pico_sync/critical_section.c \
