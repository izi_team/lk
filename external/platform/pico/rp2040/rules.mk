LOCAL_DIR := $(GET_LOCAL_DIR)

GLOBAL_INCLUDES += \
	$(LOCAL_DIR)/hardware_regs/include \
	$(LOCAL_DIR)/hardware_structs/include
