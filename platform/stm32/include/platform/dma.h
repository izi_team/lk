/*
 * Copyright (c) 2016 Erik Gilling
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT
 */

#pragma once

/**
 * dmi_init
 *
 * Initialize the DMA peripheral.
 */
void dma_init_early(void);
void dma_init(void);


