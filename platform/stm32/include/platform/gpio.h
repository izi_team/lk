/*
 * Copyright (c) 2020-2022 IZITRON (Mihail CHERCIU)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <platform/platform_cm.h>

/* -----------flags for mode-------------- */
/*|---- ---- ---- ---- xxxx xxxx ---- ---x|*/
#define GPIO_MODE           (1 << 0)
#define GPIO_MODE_IN        ((GPIO_MODE_INPUT << 8) | GPIO_MODE)
#define GPIO_MODE_OUT_PP    ((GPIO_MODE_OUTPUT_PP << 8) | GPIO_MODE)
#define GPIO_MODE_OUT_OD    ((GPIO_MODE_OUTPUT_OD << 8) | GPIO_MODE)
#define GPIO_MODE_ALT_F_PP  ((GPIO_MODE_AF_PP << 8) | GPIO_MODE)
#define GPIO_MODE_ALT_F_OD  ((GPIO_MODE_AF_OD << 8) | GPIO_MODE)
#define GPIO_MODE_ANA       ((GPIO_MODE_ANALOG << 8) | GPIO_MODE)
#define GPIO_GET_MODE(flag) (((flag) >> 8) & 0xff)

/* -----------flags for pull-------------- */
/*|---- ---- ---- xxxx ---- ---- ---- --x-|*/
#define GPIO_PULL           (1 << 1)
#define GPIO_PULL_NO        ((GPIO_NOPULL << 16) | GPIO_PULL)
#define GPIO_PULL_UP        ((GPIO_PULLUP << 16) | GPIO_PULL)
#define GPIO_PULL_DOWN      ((GPIO_PULLDOWN << 16) | GPIO_PULL)
#define GPIO_GET_PULL(flag) (((flag) >> 16) & 0xf)

/* -----------flags for speed------------- */
/*|---- ---- xxxx ---- ---- ---- ---- -x--|*/
#define GPIO_SPEED          (1 << 2)
#define GPIO_SPEED_L        ((GPIO_SPEED_FREQ_LOW << 20) | GPIO_SPEED)
#define GPIO_SPEED_M        ((GPIO_SPEED_FREQ_MEDIUM << 20) | GPIO_SPEED)
#define GPIO_SPEED_H        ((GPIO_SPEED_FREQ_HIGH << 20) | GPIO_SPEED)
#define GPIO_SPEED_VH       ((GPIO_SPEED_FREQ_VERY_HIGH << 20) | GPIO_SPEED)
#define GPIO_GET_SPEED(flag) (((flag) >> 20) & 0xf)

/* ------------flags for af--------------- */
/*|xxxx ---- ---- ---- ---- ---- ---- x---|*/
#define GPIO_AF             (1 << 3)
#define GPIO_AFn(n)         (((n) << 28) | GPIO_AF)
#define GPIO_GET_AF(flag)   (((flag) >> 28) & 0xf)

/* ------------flags for int-------------- */
/*|---- --xx ---- ---- ---- ---- ---x ----|*/
#define GPIO_INT_CFG         0x10010000
#define GPIO_INT             (1 << 4)
#define GPIO_MODE_IT_RIS     ((0x01 << 24) | GPIO_INT)
#define GPIO_MODE_IT_FAL     ((0x02 << 24) | GPIO_INT)
#define GPIO_MODE_IT_RIS_FAL ((0x03 << 24) | GPIO_INT)
#define GPIO_GET_INT(flag)   (((flag) >> 24) & 0x0f)

/* ------------flags for evt-------------- */
/*|---- xx-- ---- ---- ---- ---- --x- ----|*/
#define GPIO_EVT_CFG         0x10020000
#define GPIO_EVT             (1 << 5)
#define GPIO_MODE_EVT_RIS     ((0x01 << 26) | GPIO_EVT)
#define GPIO_MODE_EVT_FAL     ((0x02 << 26) | GPIO_EVT)
#define GPIO_MODE_EVT_RIS_FAL ((0x03 << 26) | GPIO_EVT)
#define GPIO_GET_EVT(flag)    (((flag) >> 26) & 0x0f)

/* gpio port/pin is packed into a single unsigned int in 16x:8port:8pin format */
#define GPIO(port, pin) ((unsigned int)(((port) << 8) | (pin)))
#define GPIO_PORT(gpio) (((gpio) >> 8) & 0xff)
#define GPIO_PIN(gpio) ((gpio) & 0xff)

#ifdef GPIOA
#define GPIO_PORT_A 0
#endif
#ifdef GPIOB
#define GPIO_PORT_B 1
#endif
#ifdef GPIOC
#define GPIO_PORT_C 2
#endif
#ifdef GPIOD
#define GPIO_PORT_D 3
#endif
#ifdef GPIOE
#define GPIO_PORT_E 4
#endif
#ifdef GPIOF
#define GPIO_PORT_F 5
#endif
#ifdef GPIOG
#define GPIO_PORT_G 6
#endif
#ifdef GPIOH
#define GPIO_PORT_H 7
#endif
#ifdef GPIOI
#define GPIO_PORT_I 8
#endif
#ifdef GPIOJ
#define GPIO_PORT_J 9
#endif
#ifdef GPIOK
#define GPIO_PORT_K 10
#endif
