/*
 * Copyright (c) 2016 Erik Gilling
 * Copyright (c) 2020 IZITRON (Mihail CHERCIU)
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT
 */

#include <platform/dma.h>
#include <platform/platform_cm.h>

void dma_init_early(void)
{
#if defined (STM32F030)
    __HAL_RCC_DMA1_CLK_ENABLE();
    HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
#else
    __HAL_RCC_DMA2_CLK_ENABLE();
    HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
#endif
}

void dma_init(void)
{

}
