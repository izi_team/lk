/*
 * Copyright (c) 2012 Travis Geiselbrecht
 * Copyright (c) 2016 Erik Gilling
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT
 */

#include <assert.h>
#include <dev/gpio.h>
#include <platform/gpio.h>
#include <platform/platform_cm.h>

typedef GPIO_TypeDef stm32_gpio_t;

static stm32_gpio_t *stm32_gpio_port_to_pointer(unsigned int port) {
    switch (port) {
#ifdef GPIOA
        case GPIO_PORT_A:
            return GPIOA;
#endif
#ifdef GPIOB
        case GPIO_PORT_B:
            return GPIOB;
#endif
#ifdef GPIOC
        case GPIO_PORT_C:
            return GPIOC;
#endif
#ifdef GPIOD
        case GPIO_PORT_D:
            return GPIOD;
#endif
#ifdef GPIOE
        case GPIO_PORT_E:
            return GPIOE;
#endif
#ifdef GPIOF
        case GPIO_PORT_F:
            return GPIOF;
#endif
#ifdef GPIOG
        case GPIO_PORT_G:
            return GPIOG;
#endif
#ifdef GPIOH
        case GPIO_PORT_H:
            return GPIOH;
#endif
#ifdef GPIOI
        case GPIO_PORT_I:
            return GPIOI;
#endif
#ifdef GPIOJ
        case GPIO_PORT_J:
            return GPIOJ;
#endif
#ifdef GPIOK
        case GPIO_PORT_K:
            return GPIOK;
#endif
        default:
            return NULL;
    }
}

static void stm32_gpio_enable_port(unsigned int port) {
    switch (port) {
#ifdef GPIOA
        case GPIO_PORT_A:
            __HAL_RCC_GPIOA_CLK_ENABLE();
            break;
#endif
#ifdef GPIOB
        case GPIO_PORT_B:
            __HAL_RCC_GPIOB_CLK_ENABLE();
            break;
#endif
#ifdef GPIOC
        case GPIO_PORT_C:
            __HAL_RCC_GPIOC_CLK_ENABLE();
            break;
#endif
#ifdef GPIOD
        case GPIO_PORT_D:
            __HAL_RCC_GPIOD_CLK_ENABLE();
            break;
#endif
#ifdef GPIOE
        case GPIO_PORT_E:
            __HAL_RCC_GPIOE_CLK_ENABLE();
            break;
#endif
#ifdef GPIOF
        case GPIO_PORT_F:
            __HAL_RCC_GPIOF_CLK_ENABLE();
            break;
#endif
#ifdef GPIOG
        case GPIO_PORT_G:
            __HAL_RCC_GPIOG_CLK_ENABLE();
            break;
#endif
#ifdef GPIOH
        case GPIO_PORT_H:
            __HAL_RCC_GPIOH_CLK_ENABLE();
            break;
#endif
#ifdef GPIOI
        case GPIO_PORT_I:
            __HAL_RCC_GPIOI_CLK_ENABLE();
            break;
#endif
#ifdef GPIOJ
        case GPIO_PORT_J:
            __HAL_RCC_GPIOJ_CLK_ENABLE();
            break;
#endif
#ifdef GPIOK
        case GPIO_PORT_K:
            __HAL_RCC_GPIOK_CLK_ENABLE();
            break;
#endif
        default:
            break;
    }
}


int gpio_config(unsigned nr, unsigned flags) {
    GPIO_InitTypeDef gpio_init = {0};

    uint32_t gpio_port_nr = GPIO_PORT(nr);
    uint32_t gpio_pin_nr = GPIO_PIN(nr);
    uint32_t gpio_int_evt = 0;
    assert(gpio_pin_nr < 16);

    stm32_gpio_t *gpio_port = stm32_gpio_port_to_pointer(gpio_port_nr);
    if (gpio_port == NULL) return 0;

    stm32_gpio_enable_port(gpio_port_nr);

    gpio_init.Pin = (1 << gpio_pin_nr);
    if (flags & GPIO_MODE) {
        gpio_init.Mode = GPIO_GET_MODE(flags);
    }
    if (flags & GPIO_INT) {
        gpio_int_evt = GPIO_INT_CFG;
        gpio_int_evt |= (GPIO_GET_INT(flags) << 20);
        gpio_init.Mode |= gpio_int_evt;
    }
    if (flags & GPIO_EVT) {
        gpio_int_evt = GPIO_EVT_CFG;
        gpio_int_evt |= (GPIO_GET_EVT(flags) << 20);
        gpio_init.Mode |= gpio_int_evt;
    }
    if (flags & GPIO_PULL) {
        gpio_init.Pull = GPIO_GET_PULL(flags);
    }
    if (flags & GPIO_SPEED) {
        gpio_init.Speed = GPIO_GET_SPEED(flags);
    }
    if (flags & GPIO_AF) {
         gpio_init.Alternate = GPIO_GET_AF(flags);
    }
    HAL_GPIO_Init(gpio_port, &gpio_init);

    return 0;
}

void gpio_set(unsigned nr, unsigned on) {
    HAL_GPIO_WritePin(stm32_gpio_port_to_pointer(GPIO_PORT(nr)), 1 << GPIO_PIN(nr), on);
}

int gpio_get(unsigned nr) {
    return HAL_GPIO_ReadPin(stm32_gpio_port_to_pointer(GPIO_PORT(nr)), 1 << GPIO_PIN(nr));
}

void gpio_toggle(unsigned nr) {
    HAL_GPIO_TogglePin(stm32_gpio_port_to_pointer(GPIO_PORT(nr)), 1 << GPIO_PIN(nr));
}
