# Code shared across different stm32 platforms.

LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

MODULE_SRCS += \
	$(LOCAL_DIR)/power.c \
	$(LOCAL_DIR)/debug.c \
	$(LOCAL_DIR)/gpio.c \
	$(LOCAL_DIR)/dma.c \
	$(LOCAL_DIR)/rtc.c

ifneq ($(STM32_CHIP),stm32f769)
	MODULE_SRCS += $(LOCAL_DIR)/uart.c
endif

include make/module.mk
