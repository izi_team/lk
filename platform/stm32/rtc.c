/*
 * Copyright (c) 2016 Erik Gilling
 * Copyright (c) 2020 IZITRON (Mihail CHERCIU)
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT
 */

#include <lk/debug.h>
#include <platform/rtc.h>
#include <platform/platform_cm.h>

RTC_HandleTypeDef rtc;

/* This function is called by HAL_RTC_Init() */
void HAL_RTC_MspInit(RTC_HandleTypeDef* hrtc)
{
    switch ((uintptr_t)hrtc->Instance) {
        case (uintptr_t)RTC:
            __HAL_RCC_RTC_ENABLE();
#if defined (STM32F030)
            HAL_NVIC_SetPriority(RTC_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(RTC_IRQn);
#else
            HAL_NVIC_SetPriority(RTC_WKUP_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);
#endif
            break;

        default:
            panic("unimplemented rtc instance\n");
    }

}

void rtc_init_early(void)
{
    RTC_TimeTypeDef time = {0};
    RTC_DateTypeDef date = {0};

    /* set RTC instance */
    rtc.Instance = RTC;
    rtc.Init.HourFormat = RTC_HOURFORMAT_24;
    rtc.Init.AsynchPrediv = 127;
    rtc.Init.SynchPrediv = 255;
    rtc.Init.OutPut = RTC_OUTPUT_DISABLE;
    rtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
    rtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
    if (HAL_RTC_Init(&rtc) != HAL_OK)
    {
        Error_Handler();
    }

    /* set RTC Time */
    time.Hours   = 0x0;
    time.Minutes = 0x0;
    time.Seconds = 0x0;
    time.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
    time.StoreOperation = RTC_STOREOPERATION_RESET;
    if (HAL_RTC_SetTime(&rtc, &time, RTC_FORMAT_BCD) != HAL_OK)
    {
        Error_Handler();
    }

    /* set RTC Date */
    date.WeekDay = RTC_WEEKDAY_MONDAY;
    date.Month = RTC_MONTH_JANUARY;
    date.Date = 0x1;
    date.Year = 0x0;
    if (HAL_RTC_SetDate(&rtc, &date, RTC_FORMAT_BCD) != HAL_OK)
    {
        Error_Handler();
    }
}

void rtc_init(void)
{
#ifdef RTC_WAKEUP
#ifndef RTC_WAKEUP_COUNTER
#error "Must define RTC_WAKEUP_COUNTER"
#endif
rtc_wakeup((const uint32_t)RTC_WAKEUP_COUNTER);
#endif

}

void stm32_RTC_IRQ(void) {
    HAL_RTCEx_WakeUpTimerIRQHandler(&rtc);
}

void rtc_wakeup(uint32_t ms_wakeup) {
    float time_base = 0.4; // RTC_DIV_16 / 40kHz = 0.4ms
    uint32_t rtc_cnt;

    rtc_cnt = (uint32_t)(ms_wakeup / time_base);
    printf("counter = %d, ms = %d\n", rtc_cnt, ms_wakeup);

    if (HAL_RTCEx_SetWakeUpTimer_IT(&rtc, rtc_cnt, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
    {
        Error_Handler();
    }
}

void rtc_stop_wakeup(void) {
    if (HAL_RTCEx_DeactivateWakeUpTimer(&rtc) != HAL_OK)
    {
        Error_Handler();
    }
}

lk_time_t rtc_get_timestamp(void) {
    RTC_TimeTypeDef time;
    RTC_DateTypeDef date;
    lk_time_t timestamp;

    HAL_RTC_GetTime(&rtc, &time, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(&rtc, &date, RTC_FORMAT_BIN);

    printf("___h %d\n", time.Hours);
    printf("___min %d\n", time.Minutes);
    printf("___sec %d\n", time.Seconds);

    timestamp = (time.Hours * 3600) +
                (time.Minutes * 60) +
                (time.Seconds);

    return timestamp;
}
