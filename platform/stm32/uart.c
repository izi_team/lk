/*
 * Copyright (c) 2012 Kent Ryhorchuk
 * Copyright (c) 2016 Erik Gilling
 * Copyright (c) 2020 IZITRON (Mihail CHERCIU)
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT
 */

#include <lib/cbuf.h>
#include <lk/err.h>
#include <platform/platform_cm.h>

#define DEFAULT_FLOWCONTROL UART_HWCONTROL_NONE
#define DEFAULT_BAUDRATE 115200
#define DEFAULT_RXBUF_SIZE 256

#define NUM_UARTS 6

struct uart_instance {
    UART_HandleTypeDef handle;
    cbuf_t rx_buf;
    uint8_t rx_byte;
    uint8_t tx_byte;
};

#if ENABLE_UART1
static struct uart_instance uart1;
#ifndef UART1_FLOWCONTROL
#define UART1_FLOWCONTROL DEFAULT_FLOWCONTROL
#endif
#ifndef UART1_BAUDRATE
#define UART1_BAUDRATE DEFAULT_BAUDRATE
#endif
#ifndef UART1_RXBUF_SIZE
#define UART1_RXBUF_SIZE DEFAULT_RXBUF_SIZE
#endif
#endif

#if ENABLE_UART2
static struct uart_instance uart2;
#ifndef UART2_FLOWCONTROL
#define UART2_FLOWCONTROL DEFAULT_FLOWCONTROL
#endif
#ifndef UART2_BAUDRATE
#define UART2_BAUDRATE DEFAULT_BAUDRATE
#endif
#ifndef UART2_RXBUF_SIZE
#define UART2_RXBUF_SIZE DEFAULT_RXBUF_SIZE
#endif
#endif

#if ENABLE_UART3
static struct uart_instance uart3;
#ifndef UART3_FLOWCONTROL
#define UART3_FLOWCONTROL DEFAULT_FLOWCONTROL
#endif
#ifndef UART3_BAUDRATE
#define UART3_BAUDRATE DEFAULT_BAUDRATE
#endif
#ifndef UART3_RXBUF_SIZE
#define UART3_RXBUF_SIZE DEFAULT_RXBUF_SIZE
#endif
#endif

#if ENABLE_UART4 || ENABLE_UART5 || ENABLE_UART6 || ENABLE_UART7 || ENABLE_UART8
#error add support for additional uarts
#endif

static struct uart_instance *const uart[NUM_UARTS + 1] = {
#if ENABLE_UART1
    [1] = &uart1,
#endif
#if ENABLE_UART2
    [2] = &uart2,
#endif
#if ENABLE_UART3
    [3] = &uart3,
#endif
};

/* This function is called by HAL_UART_Init() */
void HAL_UART_MspInit(UART_HandleTypeDef *huart) {

    switch ((uintptr_t)huart->Instance) {
        case (uintptr_t)USART1:
            __HAL_RCC_USART1_CLK_ENABLE();
            HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(USART1_IRQn);
            break;
        case (uintptr_t)USART2:
            __HAL_RCC_USART2_CLK_ENABLE();
            HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(USART2_IRQn);
            break;
        case (uintptr_t)USART3:
            __HAL_RCC_USART3_CLK_ENABLE();
#if defined (STM32F030)
            HAL_NVIC_SetPriority(USART3_6_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(USART3_6_IRQn);
#else
            HAL_NVIC_SetPriority(USART3_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(USART3_IRQn);
#endif
            break;
        default:
            panic("unimplemented uart\n");
    }
}

static void usart_init_early(struct uart_instance *u, USART_TypeDef *usart, uint32_t baud, uint16_t flowcontrol) {
    u->handle.Instance = usart;
    u->handle.Init.BaudRate = baud;
    u->handle.Init.WordLength = UART_WORDLENGTH_8B;
    u->handle.Init.StopBits = UART_STOPBITS_1;
    u->handle.Init.Parity = UART_PARITY_NONE;
    u->handle.Init.Mode = UART_MODE_TX_RX;
    u->handle.Init.HwFlowCtl = flowcontrol;
    u->handle.Init.OverSampling = UART_OVERSAMPLING_16;
#if defined (STM32F030)
    u->handle.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
    u->handle.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
#endif

    HAL_UART_Init(&u->handle);
}

void uart_init_early(void) {
#if ENABLE_UART1
    usart_init_early(uart[1], USART1, UART1_BAUDRATE, UART1_FLOWCONTROL);
#endif
#if ENABLE_UART2
    usart_init_early(uart[2], USART2, UART2_BAUDRATE, UART2_FLOWCONTROL);
#endif
#if ENABLE_UART3
    usart_init_early(uart[3], USART3, UART3_BAUDRATE, UART3_FLOWCONTROL);
#endif
}

static void usart_init(struct uart_instance *u, size_t buffer_size) {
    cbuf_initialize(&u->rx_buf, buffer_size);

    /* Start receive one byte in interrupt mode */
    HAL_UART_Receive_IT(&u->handle, &u->rx_byte, 1);
}

void uart_init(void) {
#ifdef ENABLE_UART1
    usart_init(uart[1], UART1_RXBUF_SIZE);
#endif
#ifdef ENABLE_UART2
    usart_init(uart[2], UART2_RXBUF_SIZE);
#endif
#ifdef ENABLE_UART3
    usart_init(uart[3], UART3_RXBUF_SIZE);
#endif
}

#if ENABLE_UART1
void stm32_USART1_IRQ(void) {
    HAL_UART_IRQHandler(&uart1.handle);
}
#endif

#if ENABLE_UART2
void stm32_USART2_IRQ(void) {
    HAL_UART_IRQHandler(&uart2.handle);
}
#endif

#if ENABLE_UART3
#if defined (STM32F030)
    void stm32_USART3_6_IRQ(void) {
#else
    void stm32_USART3_IRQ(void) {
#endif
    HAL_UART_IRQHandler(&uart3.handle);
}

#endif

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    int port = -1;

#if ENABLE_UART1
    if (huart->Instance == USART1)
    {
        port = 1;
    }
#endif
#if ENABLE_UART1
    if (huart->Instance == USART2)
    {
        port = 2;
    }
#endif
#if ENABLE_UART3
    if (huart->Instance == USART3)
    {
        port = 3;
    }
#endif

    if (port == -1) return;

    struct uart_instance *u = uart[port];
    cbuf_t *cbuf_rx = &u->rx_buf;
    uint8_t c = u->rx_byte;

    cbuf_write_char(cbuf_rx, c, false);

    /* Restart receive one byte in interrupt mode */
    HAL_UART_Receive_IT(&u->handle, &u->rx_byte, 1);
}

int uart_putc(int port, char c) {
    struct uart_instance *u = uart[port];
    if (port < 0 || port > NUM_UARTS || !u)
        return ERR_BAD_HANDLE;

    u->tx_byte = c;

    /* Start transmit one byte in non interrupt mode */
    HAL_UART_Transmit(&u->handle, &u->tx_byte, 1, 10);
    /* Restart receive one byte in interrupt mode */
    HAL_UART_Receive_IT(&u->handle, &u->rx_byte, 1);

    return 1;
}

int uart_getc(int port, bool wait) {
    struct uart_instance *u = uart[port];
    if (port < 0 || port > NUM_UARTS || !u)
        return ERR_BAD_HANDLE;

    char c;
    if (cbuf_read_char(&u->rx_buf, &c, wait) == 0)
        return ERR_IO;
    return c;
}
