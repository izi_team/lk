#include "pico.h"
#include <hardware/gpio.h>
#include <hardware/pwm.h>

void pwm(int pin,int channel, float duty_cycle);
void set_resolution(int pin, int resolution);
