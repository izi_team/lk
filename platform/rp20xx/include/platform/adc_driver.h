#include "pico.h"
#include <hardware/gpio.h>
#include <hardware/adc.h>

#define TEMPERATURE_CHANNEL 4


uint16_t analog_read(int pin, int channel);
uint16_t temperature_read();
