#include <pico.h>
#include <hardware/pll.h>
#include <hardware/clocks.h>
#include <hardware/xosc.h>
#include <hardware/regs/io_bank0.h>
#include <hardware/structs/scb.h>
#include <hardware/gpio.h>

#define WAKEUP_PIN      24

void enter_sleep_mode(bool edge,bool high);
void low_power_run(enum clock_index clock_array []);