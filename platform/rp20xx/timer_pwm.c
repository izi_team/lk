#include <platform/timer_pwm.h>

static int  rp_set_resolution(int pin, int resolution);
static void rp_pwm(int pin,int channel, float duty_cycle);



static void rp_pwm(int pin,int channel, float duty_cycle){
    int ccr ;
    // allocate the gpio to PWM
    gpio_set_function(pin,GPIO_FUNC_PWM);
    
    //get the slice connected to gpio
    uint slice_num=pwm_gpio_to_slice_num(pin);
    // set resolution (10 bit)
    int arr_value=rp_set_resolution(pin,10);
    // set the duty cycle
    ccr=(duty_cycle/100)*arr_value;

    pwm_set_chan_level(slice_num,channel,ccr);

    // start pwm 
    pwm_set_enabled(slice_num,true);

}

static int rp_set_resolution(int pin, int resolution){
    //get the slice connected to gpio
    uint slice_num=pwm_gpio_to_slice_num(pin);

    // set resolution of timer
    uint16_t arr_value=1;
    for(int i=0;i<resolution;i++)
        arr_value*=2;
    arr_value=arr_value-1;

    pwm_set_wrap(slice_num,arr_value);
    return arr_value;

}



void pwm(int pin,int channel,float duty_cycle){
    rp_pwm(pin,channel,duty_cycle);
}

void set_resolution(int pin, int resolution){
    rp_set_resolution(pin,resolution);
}