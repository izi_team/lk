// Copyright (c) 2022 IZITRON
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT

#include <hardware/i2c.h>
#include <kernel/mutex.h>
#include <lk/err.h>

#define NUM_I2C 2

struct i2c_instance {
    i2c_inst_t *handle;
    mutex_t lock;
};

#if ENABLE_I2C0
static struct i2c_instance ins_i2c0 = {
    .handle = i2c0,
    .lock = NULL
};
#endif

#if ENABLE_I2C1
static struct i2c_instance ins_i2c1 = {
    .handle = i2c1,
    .lock = NULL
};
#endif

static struct i2c_instance *const i2c[NUM_I2C] = {
#if ENABLE_I2C0
    [0] = &ins_i2c0,
#endif
#if ENABLE_I2C1
    [1] = &ins_i2c1,
#endif
};

void i2c_init_early(void) {
#ifdef ENABLE_I2C0
    i2c_init(i2c[0]->handle, 100000);
    mutex_init(&i2c[0]->lock);
#endif

#ifdef ENABLE_I2C1
    i2c_init(i2c[1]->handle, 100000);
    mutex_init(&i2c[1]->lock);
#endif
}

status_t i2c_transmit(int bus, uint8_t address, const void *buf, size_t count) {
    int ret = ERR_I2C_NACK;

    struct i2c_instance *i2c_ins = i2c[bus];
    if (bus < 0 || bus > NUM_I2C || !i2c_ins)
        return ERR_BAD_HANDLE;

    mutex_acquire(&i2c_ins->lock);
    ret = i2c_write_blocking(i2c_ins->handle, address, buf, count, false);
    mutex_release(&i2c_ins->lock);

    if (ret <= 0)
        ret = ERR_I2C_NACK;
    else
        ret = NO_ERROR;

    return ret;
}

status_t i2c_receive(int bus, uint8_t address, void *buf, size_t count) {
    uint8_t ret = ERR_I2C_NACK;

    struct i2c_instance *i2c_ins = i2c[bus];
    if (bus < 0 || bus > NUM_I2C || !i2c_ins)
        return ERR_BAD_HANDLE;

    mutex_acquire(&i2c_ins->lock);
    ret = i2c_read_blocking(i2c_ins->handle, address, buf, count, false);
    mutex_release(&i2c_ins->lock);

    if (ret <= 0)
        ret = ERR_I2C_NACK;
    else
        ret = NO_ERROR;

    return ret;
}