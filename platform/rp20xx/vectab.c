// Copyright (c) 2020 Brian Swetland
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT

#include <lk/debug.h>
#include <lk/compiler.h>
#include <arch/arm/cm.h>

/* un-overridden irq handler */
void rp20xx_dummy_irq(uint8_t nr) {
    arm_cm_irq_entry();
    printf("unhandled irq %d\n", nr);
}
void rp20xx_dummy_irq_0(void) {
    rp20xx_dummy_irq(0);
}
void rp20xx_dummy_irq_1(void) {
    rp20xx_dummy_irq(1);
}
void rp20xx_dummy_irq_2(void) {
    rp20xx_dummy_irq(2);
}
void rp20xx_dummy_irq_3(void) {
    rp20xx_dummy_irq(3);
}
void rp20xx_dummy_irq_4(void) {
    rp20xx_dummy_irq(4);
}
void rp20xx_dummy_irq_5(void) {
    rp20xx_dummy_irq(5);
}
void rp20xx_dummy_irq_6(void) {
    rp20xx_dummy_irq(6);
}
void rp20xx_dummy_irq_7(void) {
    rp20xx_dummy_irq(7);
}
void rp20xx_dummy_irq_8(void) {
    rp20xx_dummy_irq(8);
}
void rp20xx_dummy_irq_9(void) {
    rp20xx_dummy_irq(9);
}
void rp20xx_dummy_irq_10(void) {
    rp20xx_dummy_irq(10);
}
void rp20xx_dummy_irq_11(void) {
    rp20xx_dummy_irq(11);
}
void rp20xx_dummy_irq_12(void) {
    rp20xx_dummy_irq(12);
}
void rp20xx_dummy_irq_13(void) {
    rp20xx_dummy_irq(13);
}
void rp20xx_dummy_irq_14(void) {
    rp20xx_dummy_irq(14);
}
void rp20xx_dummy_irq_15(void) {
    rp20xx_dummy_irq(15);
}
void rp20xx_dummy_irq_16(void) {
    rp20xx_dummy_irq(16);
}
void rp20xx_dummy_irq_17(void) {
    rp20xx_dummy_irq(17);
}
void rp20xx_dummy_irq_18(void) {
    rp20xx_dummy_irq(18);
}
void rp20xx_dummy_irq_19(void) {
    rp20xx_dummy_irq(19);
}
void rp20xx_dummy_irq_20(void) {
    rp20xx_dummy_irq(20);
}
void rp20xx_dummy_irq_21(void) {
    rp20xx_dummy_irq(21);
}
void rp20xx_dummy_irq_22(void) {
    rp20xx_dummy_irq(22);
}
void rp20xx_dummy_irq_23(void) {
    rp20xx_dummy_irq(23);
}
void rp20xx_dummy_irq_24(void) {
    rp20xx_dummy_irq(24);
}
void rp20xx_dummy_irq_25(void) {
    rp20xx_dummy_irq(25);
}


// #define RP20XX_IRQ(name,num) \
// void num##rp20xx_dummy_irq(void) { \
//     arm_cm_irq_entry(); \
//     printf("unhandled irq %d\n", num##); \
// } \
// #include <platform/irqinfo.h>
// #undef RP20XX_IRQ

/* a list of default handlers that are simply aliases to the dummy handler */
#define RP20XX_IRQ(name,num) \
void name##_IRQHandler(void) __WEAK_ALIAS("rp20xx_dummy_irq_"#num);
#include <platform/irqinfo.h>
#undef RP20XX_IRQ

const void* const __SECTION(".text.boot.vectab2") vectab2[26] = {
#define RP20XX_IRQ(name,num) [name##_IRQn] = name##_IRQHandler,
#include <platform/irqinfo.h>
#undef RP20XX_IRQ
};

