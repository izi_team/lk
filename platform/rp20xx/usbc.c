// Copyright (c) 2021 Mihail CHERCIU (mihail.cherciu@izitron.com)
//
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT

#include <app.h>
#include "system.h"
#include "tusb.h"
#include "platform/usbc.h"

const uint8_t *tud_descriptor_device_cb(void) {
    return (const uint8_t *)&usbd_desc_device;
}

const uint8_t *tud_descriptor_configuration_cb(uint8_t index) {
    (void)index;
    return usbd_desc_cfg;
}

const uint16_t *tud_descriptor_string_cb(uint8_t index, uint16_t langid) {
    #define DESC_STR_MAX (20)
    static uint16_t desc_str[DESC_STR_MAX];

    uint8_t len;
    if (index == 0) {
        desc_str[1] = 0x0409; // supported language is English
        len = 1;
    } else {
        if (index >= sizeof(usbd_desc_str) / sizeof(usbd_desc_str[0])) {
            return NULL;
        }
        const char *str = usbd_desc_str[index];
        for (len = 0; len < DESC_STR_MAX - 1 && str[len]; ++len) {
            desc_str[1 + len] = str[len];
        }
    }

    // first byte is length (including header), second byte is string type
    desc_str[0] = (TUSB_DESC_STRING << 8) | (2 * len + 2);

    return desc_str;
}

void usb_out_chars(const char *buf, int length) {

    if (tud_cdc_connected()) {
        tud_cdc_write(buf,length);
        tud_task();
        tud_cdc_write_flush();
    }
}

int usb_in_chars(char *buf, int length) {
    uint32_t owner;

    int rc = -1;
    if (tud_cdc_connected() && tud_cdc_available()) {
        int count = tud_cdc_read(buf, length);
        rc =  count ? count : -1;
    }
    return rc;
}

static void usb_init(const struct app_descriptor *app)
{
    /* Init system */
    printf("Init USB Manager...\n");
    tusb_init();
}

static void usb_entry(const struct app_descriptor *app, void *args)
{
    thread_set_priority(HIGHEST_PRIORITY);

    while (true) {
        thread_sleep(10);
        tud_task();
    }
}

APP_START(usb)
    .init   = usb_init,
    .entry  = usb_entry,
APP_END

//tud_int_handler(BOARD_DEVICE_RHPORT_NUM);