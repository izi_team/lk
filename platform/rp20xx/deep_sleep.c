#include <platform/deep_sleep.h>



static void sleep_until_pin_wakeup( bool edge, bool high){



    bool low   =!high;
    bool level =!edge;
    // configure the appropriate IRQ at bank 0
    assert(WAKEUP_PIN < NUM_BANK0_GPIOS);

    uint32_t event = 0;

    if(level && low ) event = IO_BANK0_DORMANT_WAKE_INTE3_GPIO24_LEVEL_LOW_BITS;
    if(level && low ) event = IO_BANK0_DORMANT_WAKE_INTE3_GPIO24_LEVEL_HIGH_BITS;
    if(level && low ) event = IO_BANK0_DORMANT_WAKE_INTE3_GPIO24_EDGE_HIGH_BITS;
    if(level && low ) event = IO_BANK0_DORMANT_WAKE_INTE3_GPIO24_EDGE_LOW_BITS;

    //gpio_set_dormant_irq_enabled(WAKEUP_PIN,event,true);

    // put the xosc into dormant mode 
    // which stop all processor axecution 
 
    xosc_dormant();
    // stop all other clocked logic on the chip

    

    // Execution stops here until woken up

    // clear the irq so dormant mode can be entered again 

    gpio_acknowledge_irq(WAKEUP_PIN,event);


}

static void disable_perepheral_clock(enum clock_index clock_array []){
    /*
    clk_gpout0 = 0,     ///< GPIO Muxing 0
    clk_gpout1,         ///< GPIO Muxing 1
    clk_gpout2,         ///< GPIO Muxing 2
    clk_gpout3,         ///< GPIO Muxing 3
    clk_ref,            ///< Watchdog and timers reference clock
    clk_sys,            ///< Processors, bus fabric, memory, memory mapped registers
    clk_peri,           ///< Peripheral clock for UART and SPI
    clk_usb,            ///< USB clock
    clk_adc,            ///< ADC clock
    clk_rtc,            ///< Real time clock
    */
   for(int i=0;i<sizeof(clock_array)/sizeof(clock_array[0]);i++)
        clock_stop(clock_array[i]);

}



void enter_sleep_mode(bool edge, bool high){

    sleep_until_pin_wakeup(edge,high);
}


void low_power_run(enum clock_index clock_array []){
    disable_perepheral_clock( clock_array );
}