#include <platform/adc_driver.h>


// rp2040 has only one ADC (12-bit)
// for rp2040 ther's 4 ADC pins but only 3 are usable

/* ADC Module           GPIO Pins   */
//  ADC0                GPIO26 
//  ADC1                GPIO27



static uint16_t rp_adc_read(int pin, int channel){

    uint16_t value =0;
    // enable adc perepheral
    adc_init();

    // init the adc pin 
    // make sure it's high-impedance ( no pullup or pulldown)
    adc_gpio_init(pin);

    // select the corresponding ADC input 
    // input 0 for GPIO26
    // input 1 for GPIO27

    adc_select_input(channel);
    

    value = adc_read();
    return value ;

}


static uint16_t get_tempertaure(){
    adc_init();
    adc_set_temp_sensor_enabled(true);
    adc_select_input(TEMPERATURE_CHANNEL);
    return adc_read();
}
 

uint16_t analog_read(int pin, int channel){
    return rp_adc_read(pin,channel);
}

uint16_t temperature_read(){
    return get_tempertaure();
}
