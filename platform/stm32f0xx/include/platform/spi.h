/*
 * Copyright (c) 2016 Erik Gilling
 * Copyright (c) 2020 IZITRON (Mihail CHERCIU)
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT
 */
#pragma once

#include <sys/types.h>

void spi_init_early(void);
void spi_init(void);
status_t spi_xfer(int bus, uint8_t *tx_buf, uint8_t *rx_buf, uint16_t len);
status_t spi_transmit(int bus, uint8_t *tx_buf, uint16_t len);
status_t spi_receive(int bus, uint8_t *rx_buf, uint16_t len);
