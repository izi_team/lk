#pragma once

#include <stdbool.h>
#include <stm32f0xx.h>

bool stm32_exti0_irq(void);
bool stm32_exti1_irq(void);
bool stm32_exti2_irq(void);
bool stm32_exti3_irq(void);
bool stm32_exti4_irq(void);
bool stm32_exti5_irq(void);
bool stm32_exti6_irq(void);
bool stm32_exti7_irq(void);
bool stm32_exti8_irq(void);
bool stm32_exti9_irq(void);
bool stm32_exti10_irq(void);
bool stm32_exti11_irq(void);
bool stm32_exti12_irq(void);
bool stm32_exti13_irq(void);
bool stm32_exti14_irq(void);
bool stm32_exti15_irq(void);

void exti_init_early(void);
void exti_init(void);