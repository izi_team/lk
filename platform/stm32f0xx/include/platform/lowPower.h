// impliment standby mode as low power mode


#include <arch/arm/cm.h>
#include <platform/rcc.h>
#include <stm32f0xx.h>
#include <stdbool.h>

void enter_standby_mode(void);
void enter_sleep_mode(void);
void enter_stop_mode(void);

bool check_sleep_mode(void);
void ADC_power_on(void);
void ADC_power_off(void);
