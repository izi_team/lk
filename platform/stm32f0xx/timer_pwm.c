#include <platform/timer_pwm.h>

#include <arch/arm/cm.h>
#include <assert.h>
#include <platform/rcc.h>



// create an enumeration of timers used to generate pwm signal
// this enum will help to configure each timer 
// the izirunf0 board has 4 accessible pwm timers TIM1, TIM3, TIM16 and TIM17
typedef enum {

    STM32_TIM_NOT_FOUND = -1,
    STM32_TIM1_INDEX = 0,
    STM32_TIM3_INDEX,
    STM32_TIM16_INDEX,
    STM32_TIM17_INDEX,
    STM32_NUM_TIMERS,

} stm32_timer_index_t;

// to access timer's registers 
typedef TIM_TypeDef stm32_timer_regs_t;

typedef struct stm32_timer_config_ {
    stm32_timer_regs_t *regs;       // pointer to registers
    
    stm32_rcc_clk_t clock;          // counter speed or timer frequency

    int             channels[4];

} stm32_timer_config_t;


// declare an array of timers config

stm32_timer_config_t stm32_timer_config[] = {
#ifdef TIM1
    [STM32_TIM1_INDEX] = {
        .regs = TIM1,
        .clock = STM32_RCC_CLK_TIM1,
        .channels  = {0,0,0,0},
        
    },
#endif
#ifdef TIM3
    [STM32_TIM3_INDEX] = {
        .regs = TIM3,
        .clock = STM32_RCC_CLK_TIM3,
        .channels  = {0,0,0,0},
    },
#endif
#ifdef TIM16
    [STM32_TIM16_INDEX] = {
        .regs = TIM16,
        .clock = STM32_RCC_CLK_TIM16,
        .channels  = {0,0,0,0},
    },
#endif
#ifdef TIM17
    [STM32_TIM17_INDEX] = {
        .regs = TIM17,
        .clock = STM32_RCC_CLK_TIM17,
        .channels  = {0,0,0,0},
    },
#endif
};

// function to get timer index in the array stm32_timer_config

static stm32_timer_index_t stm32_timer_get_index(int timer) {
    switch (timer) {
        case 1:
            return STM32_TIM1_INDEX;
            break;
        case 3:
            return STM32_TIM3_INDEX;
            break;
        case 16:
            return STM32_TIM16_INDEX;
            break;
        case 17:
            return STM32_TIM17_INDEX;
            break;   
    }
    return STM32_TIM_NOT_FOUND;
}





// function to set timer channel and pwm mode 
// and enable the corresponding preload register (set the OCxPE in TIMx_CCMRx register)

static void stm32_timer_pwm_setup_chan( int timer, int chan, int mode) {
    
    int shift=0;
    int index=stm32_timer_get_index(timer);                      // get index of timer
    stm32_timer_config_t *t= &stm32_timer_config[index];         // get pointer to struct stm32_timer_config_t in index
    t->channels[chan-1]=1;                                         // memorisze the channel used 
    // set the channel of the timer
    
    if((timer==3) || (timer==1)){
        shift =(chan-1)*4;
        t->regs->CCER |=(1U << shift);                           // set the CCx bit -> channel configured as output
    }
    else if((timer==16) || (timer==17)){
        t->regs->CCER |= 0x1;
    }

    uint16_t ccmr=0x0U;
    
    // set bits for mode 1 and mode 2
    switch(mode){
        case 1:
        ccmr=0x0060;
        break;
        case 2:
        ccmr=0x0070;
        break;
    }
    
    // select channel with the corresponding pwm mode
    // enable the corresponding preload register
    switch(chan){
        case 1:
        t->regs->CCMR1 |= ccmr;
        t->regs->CCMR1 |=( 1U << 3 );
        break;
        case 2:
        t->regs->CCMR1 |= ( ccmr << 8 );
        t->regs->CCMR1 |= ( 1U << 11 );
        break;
        case 3:
        t->regs->CCMR2 |= ccmr;
        t->regs->CCMR2 |=( 1U << 3 );
        break;
        case 4:
        t->regs->CCMR2 |= ( ccmr << 8 );
        t->regs->CCMR2 |= ( 1U << 11 );
        break;
    }
    // force update generation (UG=1)
    t->regs->EGR |= 0x1;
}

// function to set the pwm type :edge-aligned mode or center-aligned mode

static void stm32_timer_pwm_set_pwm_type(int timer,int type){
    // type = 0 : Edge-aligned mode
    // type = 1 : Center-aligned mode 1
    // type = 2 : Center-aligned mode 2
    // type = 3 : Center-aligned mode 3
    // chane CMS[1:0] (bits 6:5) in Timx_CR1 register to select mode

    int index=stm32_timer_get_index(timer);                  // get index of timer
    stm32_timer_config_t *t= &stm32_timer_config[index];     // get pointer to struct stm32_timer_config_t in index
    
    switch(type){
        case 0: 
        t->regs->CR1 &=~ 0x60;
        break;
        case 1:
        t->regs->CR1 &=~ 0x40;
        t->regs->CR1 |=  0x20;
        break;
        case 2:
        t->regs->CR1 &=~ 0x20;
        t->regs->CR1 |=  0x40;
        break;
        case 3:
        t->regs->CR1 |=  0x60;
        break;

    }

}

// function to set the pwm frequency ( the frequency f is given in kHz to simplify
// user input ) 

static void stm32_timer_pwm_set_frequency(int timer, int f ){

    long int prescaler_value;

    int index=stm32_timer_get_index(timer);                  // get index of timer
    stm32_timer_config_t *t= &stm32_timer_config[index];     // get pointer to struct stm32_timer_config_t in index
    
    // get the clock frequency
    long int fclk= t->clock;

    // get the auto-reloead register (ARR) value
    long int arr_value= t->regs->ARR; 

    // calculate the prescaler value 
    prescaler_value=(fclk/f)/(arr_value + 1) -1;
    //assign the prescalervalue to the prescaler  register 
    t->regs->PSC=prescaler_value - 1;
 
}




// function to set the auto-reload register based on the resolution given (in bits)
// and enable the auto-reload preload register
static void stm32_timer_pwm_set_ARR(int timer, int resolution) {

    // resolution = Log2(ARR+1)
    // calculate 2^resolution (without math.h library) 

    uint16_t arr_value=1;
    for(int i=0;i<resolution;i++)
        arr_value*=2;
    arr_value=arr_value-1;
    
    int index=stm32_timer_get_index(timer);                  // get index of timer
    stm32_timer_config_t *t= &stm32_timer_config[index];     // get pointer to struct stm32_timer_config_t in index
    
    // set the calculated value to the ARR register
    t->regs->ARR=arr_value;
    //printf("arr=%d\n",t->regs->ARR);

    //enable the auto-reload preload register by setting the ARPE bit in the TIMx_CR1
    t->regs->CR1 |= (1U << 7);

}

// function to set the duty cycle based on the ration CCRx/ARR in %
 static void stm32_timer_pwm_set_duty_cycle(int timer, int chan, int duty_cycle){

    int index=stm32_timer_get_index(timer);                  // get index of timer
    stm32_timer_config_t *t= &stm32_timer_config[index];     // get pointer to struct stm32_timer_config_t in index
    
    uint16_t arr=t->regs->ARR;
    uint16_t ccr=(duty_cycle*arr)/100;
    //printf("ccr=%d\n",ccr);
    // load the crr in the corresponding CCRx
    switch(chan){
        case 1:
        t->regs->CCR1 = ccr;
        break;
        case 2:
        t->regs->CCR2 = ccr;
        break;
        case 3:
        t->regs->CCR3 = ccr;
        break;
        case 4:
        t->regs->CCR4 = ccr;
        break;
    }

}


   
// function to start generating the pwm signal
// by setting the CR1_CEN bit 
// for advanced timers (TIM1) set also MOE (main output enable) to globally enable the PWM
static void stm32_timer_pwm_start(int timer){

    int index=stm32_timer_get_index(timer);                 // get index of timer
    stm32_timer_config_t *t= &stm32_timer_config[index];    // get pointer to struct stm32_timer_config_t in index

    if((timer==1) || (timer==16) || (timer==17) ){
        t->regs->CR1  |= (1 << 0 );                         // set CR1_CEN  bit
        t->regs->BDTR |= (1 << 15);                         // set BDTR_MOE bit
    }
    // case of TIM3 
        t->regs->CR1  |= (1 << 0 );
}

// Enable rcc 
static void enable_Tim_clock(int timer){

    switch(timer){
        case 1:
            RCC->APB2ENR |= (1U << 11);
        break;
        case 3:
            RCC->APB1ENR |= (1U << 1);
        break;
        case 16:
            RCC->APB2ENR |= (1U << 17);
        break;
        case 17:
            RCC->APB2ENR |= (1U << 18);
        break;
    }
}
    

/* function to call in other c files */


// function to print all channels used 
int* get_used_channels(int timer){
    int index=stm32_timer_get_index(timer);           
    stm32_timer_config_t *t= &stm32_timer_config[index];
    return t->channels;
}
 

// function to initialize the timer
// set a default frequency , resolution and mode

void pwm_init_chan(int timer,int chan){
    // enable rcc clock
    enable_Tim_clock(timer);
    // define mode 1 as default
    int mode=1;
    stm32_timer_pwm_setup_chan(timer,chan,mode);
    // the pwm type 0 is set by default
    // set the default resolution to 10 bits
    // set the default frequencyto 25 khz
    uint8_t f=25;
    int resolution=10;
    stm32_timer_pwm_set_ARR(timer,resolution);
    stm32_timer_pwm_set_frequency(timer,f);
}




void set_pwm_frequency(int timer,int f ){

    stm32_timer_pwm_set_frequency(timer,f);
}

void set_timer_resolution(int timer, int resolution){

    stm32_timer_pwm_set_ARR(timer,resolution);
}

void set_duty_cycle(int timer, int chan, int duty_cycle){
    stm32_timer_pwm_set_duty_cycle(timer,chan,duty_cycle);

}

void pwm_start(int timer){
    stm32_timer_pwm_start(timer);
}

void set_channel(int timer, int chan, int mode){
    stm32_timer_pwm_setup_chan(timer,chan,mode );

}

void set_type(int timer,int type){
    stm32_timer_pwm_set_pwm_type(timer,type);
}



// use pins directly