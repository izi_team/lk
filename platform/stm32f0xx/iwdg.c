/*
 * Copyright (c) 2019 Mihail CHERCIU mihail.cherciu@izitron.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#define USE_HAL_DRIVER
#include <platform/iwdg.h>
#include <stdbool.h>
#include <lk/debug.h>
#include <lk/trace.h>
#include <arch/arm/cm.h>
#include <kernel/event.h>
#include <kernel/mutex.h>
#include <platform/dma.h>
#include <platform/rcc.h>
#include <platform.h>

#define LOCAL_TRACE 1

IWDG_HandleTypeDef hiwdg;
bool iwdg_is_init = false;

HAL_StatusTypeDef iwdg_init(void)
{
    hiwdg.Instance = IWDG;
    hiwdg.Init.Prescaler = IWDG_PRESCALER_64;
    hiwdg.Init.Window = 625;
    hiwdg.Init.Reload = 625;
    if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
    {
        LTRACEF("ERROR WWDG Init\n");
        return HAL_ERROR;
    }
    iwdg_is_init = true;
    return HAL_OK;
}

HAL_StatusTypeDef iwdg_refresh(void)
{
    if (iwdg_is_init)
        return HAL_IWDG_Refresh(&hiwdg);
    else
        return HAL_ERROR;
}
