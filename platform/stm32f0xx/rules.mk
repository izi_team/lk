LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

# ROMBASE, MEMBASE, and MEMSIZE are required for the linker script
ROMBASE := 0x08000000
MEMBASE := 0x20000000
# can be overridden by target

ARCH := arm
ARM_CPU := cortex-m0

ifeq ($(STM32_CHIP),stm32f030)
GLOBAL_DEFINES += STM32F030
MEMSIZE ?= 32768
ROMSIZE ?= 262144
FOUND_CHIP := true
endif
ifeq ($(STM32_CHIP),stm32f070)
GLOBAL_DEFINES += STM32F070
MEMSIZE ?= 16384
ROMSIZE ?= 131072
FOUND_CHIP := true
endif
ifeq ($(STM32_CHIP),stm32f072)
GLOBAL_DEFINES += STM32F072
MEMSIZE ?= 16384
ROMSIZE ?= 131072
FOUND_CHIP := true
endif

ifeq ($(FOUND_CHIP),)
$(error unknown STM32F0xx chip $(STM32_CHIP))
endif

GLOBAL_DEFINES += \
	MEMSIZE=$(MEMSIZE) \
	ROMSIZE=$(ROMSIZE) \
	STM32_CHIP=\"$(STM32_CHIP)\"

MODULE_SRCS += \
	$(LOCAL_DIR)/vectab.c \
	$(LOCAL_DIR)/init.c \
	$(LOCAL_DIR)/adc.c \
	$(LOCAL_DIR)/i2c.c \
	$(LOCAL_DIR)/timer_pwm.c \
	$(LOCAL_DIR)/lowPower.c \
	$(LOCAL_DIR)/spi.c \
	$(LOCAL_DIR)/exti.c

# $(LOCAL_DIR)/usbc.c \
# $(LOCAL_DIR)/can.c \
# $(LOCAL_DIR)/timer_capture.c \
# $(LOCAL_DIR)/iwdg.c \

# use a two segment memory layout, where all of the read-only sections
# of the binary reside in rom, and the read/write are in memory. The
# ROMBASE, MEMBASE, and MEMSIZE make variables are required to be set
# for the linker script to be generated properly.
#
LINKER_SCRIPT += \
	$(BUILDDIR)/system-twosegment.ld

MODULE_DEPS += \
	platform/stm32f0xx/CMSIS \
	platform/stm32f0xx/STM32F0xx_HAL_Driver \
	platform/stm32 \
	arch/arm/arm-m/systick \
	lib/cbuf \
	dev/usb

include make/module.mk
