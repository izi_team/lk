/*
 * Copyright (c) 2016 Erik Gilling
 * Copyright (c) 2020 IZITRON (Mihail CHERCIU)
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT
 */

#include <lk/err.h>
#include <kernel/mutex.h>
#include <platform/spi.h>
#include <platform/platform_cm.h>

#define DEFAULT_SPI_MODE                SPI_MODE_MASTER
#define DEFAULT_SPI_DIRECTION_LINES     SPI_DIRECTION_2LINES
#define DEFAULT_SPI_DATASIZE            SPI_DATASIZE_8BIT
#define DEFAULT_SPI_POLARITY            SPI_POLARITY_LOW
#define DEFAULT_SPI_PHASE               SPI_PHASE_1EDGE
#define DEFAULT_SPI_NSS                 SPI_NSS_SOFT
#define DEFAULT_SPI_BAUDRATEPRESCALER   SPI_BAUDRATEPRESCALER_32
#define DEFAULT_SPI_FIRSTBIT            SPI_FIRSTBIT_MSB

#define NUM_SPI 2

struct spi_instance {
    SPI_HandleTypeDef handle;
    mutex_t lock;
};

#if ENABLE_SPI1
static struct spi_instance spi1;
#ifndef SPI1_MODE
#define SPI1_MODE DEFAULT_SPI_MODE
#endif
#ifndef SPI1_DIRECTION_LINES
#define SPI1_DIRECTION_LINES DEFAULT_SPI_DIRECTION_LINES
#endif
#ifndef SPI1_DATASIZE
#define SPI1_DATASIZE DEFAULT_SPI_DATASIZE
#endif
#ifndef SPI1_POLARITY
#define SPI1_POLARITY DEFAULT_SPI_POLARITY
#endif
#ifndef SPI1_PHASE
#define SPI1_PHASE DEFAULT_SPI_PHASE
#endif
#ifndef SPI1_NSS
#define SPI1_NSS DEFAULT_SPI_NSS
#endif
#ifndef SPI1_BAUDRATEPRESCALER
#define SPI1_BAUDRATEPRESCALER DEFAULT_SPI_BAUDRATEPRESCALER
#endif
#ifndef SPI1_FIRSTBIT
#define SPI1_FIRSTBIT DEFAULT_SPI_FIRSTBIT
#endif
#endif

#if ENABLE_SPI2
static struct spi_instance spi2;
#ifndef SPI2_MODE
#define SPI2_MODE DEFAULT_SPI_MODE
#endif
#ifndef SPI2_DIRECTION_LINES
#define SPI2_DIRECTION_LINES DEFAULT_SPI_DIRECTION_LINES
#endif
#ifndef SPI2_DATASIZE
#define SPI2_DATASIZE DEFAULT_SPI_DATASIZE
#endif
#ifndef SPI2_POLARITY
#define SPI2_POLARITY DEFAULT_SPI_POLARITY
#endif
#ifndef SPI2_PHASE
#define SPI2_PHASE DEFAULT_SPI_PHASE
#endif
#ifndef SPI2_NSS
#define SPI2_NSS DEFAULT_SPI_NSS
#endif
#ifndef SPI2_BAUDRATEPRESCALER
#define SPI2_BAUDRATEPRESCALER DEFAULT_SPI_BAUDRATEPRESCALER
#endif
#ifndef SPI2_FIRSTBIT
#define SPI2_FIRSTBIT DEFAULT_SPI_FIRSTBIT
#endif
#endif

static struct spi_instance *const spi[NUM_SPI + 1] = {
#if ENABLE_SPI1
    [1] = &spi1,
#endif
#if ENABLE_SPI2
    [2] = &spi2,
#endif
};

/* This function is called by HAL_SPI_Init() */
void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi) {
    switch ((uintptr_t)hspi->Instance) {
        case (uintptr_t)SPI1:
            __HAL_RCC_SPI1_CLK_ENABLE();
            HAL_NVIC_SetPriority(SPI1_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(SPI1_IRQn);
            break;
        case (uintptr_t)SPI2:
            __HAL_RCC_SPI2_CLK_ENABLE();
            HAL_NVIC_SetPriority(SPI2_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(SPI2_IRQn);
            break;
        default:
            panic("unimplemented spi\n");
    }
}

static void stm32_spi_early_init(struct spi_instance *spi_ins,
                                         SPI_TypeDef *hspi,
                                            uint32_t  spiMode,
                                            uint32_t  dirLine,
                                            uint32_t  dataSize,
                                            uint32_t  polarity,
                                            uint32_t  phase,
                                            uint32_t  nss,
                                            uint32_t  baudrate,
                                            uint32_t  firstBit)
{
    spi_ins->handle.Instance = hspi;
    spi_ins->handle.Init.Mode = spiMode;
    spi_ins->handle.Init.Direction = dirLine;
    spi_ins->handle.Init.DataSize = dataSize;
    spi_ins->handle.Init.CLKPolarity = polarity;
    spi_ins->handle.Init.CLKPhase = phase;
    spi_ins->handle.Init.NSS = nss;
    spi_ins->handle.Init.BaudRatePrescaler = baudrate;
    spi_ins->handle.Init.FirstBit = firstBit;
    spi_ins->handle.Init.TIMode = SPI_TIMODE_DISABLE;
    spi_ins->handle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    spi_ins->handle.Init.CRCPolynomial = 7;
    spi_ins->handle.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
    spi_ins->handle.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;

    if (HAL_SPI_Init(&spi_ins->handle) != HAL_OK)
    {
        Error_Handler();
    }

    mutex_init(&spi_ins->lock);
}

void spi_init_early(void) {
#ifdef ENABLE_SPI1
    stm32_spi_early_init(spi[1], SPI1,
                                 SPI1_MODE,
                                 SPI1_DIRECTION_LINES,
                                 SPI1_DATASIZE,
                                 SPI1_POLARITY,
                                 SPI1_PHASE,
                                 SPI1_NSS,
                                 SPI1_BAUDRATEPRESCALER,
                                 SPI1_FIRSTBIT);
#endif

#ifdef ENABLE_SPI2
    stm32_spi_early_init(spi[2], SPI2,
                                 SPI2_MODE,
                                 SPI2_DIRECTION_LINES,
                                 SPI2_DATASIZE,
                                 SPI2_POLARITY,
                                 SPI2_PHASE,
                                 SPI2_NSS,
                                 SPI2_BAUDRATEPRESCALER,
                                 SPI2_FIRSTBIT);
#endif
}

void spi_init(void) {

}

#ifdef ENABLE_SPI1
void stm32_SPI1_IRQ(void) {
    HAL_SPI_IRQHandler(&spi1.handle);
}
#endif

#ifdef ENABLE_SPI2
void stm32_SPI2_IRQ(void) {
    HAL_SPI_IRQHandler(&spi2.handle);
}
#endif

status_t spi_xfer(int bus, uint8_t *tx_buf, uint8_t *rx_buf, uint16_t len) {
    uint8_t ret = HAL_OK;

    struct spi_instance *spi_ins = spi[bus];
    if (bus < 0 || bus > NUM_SPI || !spi_ins)
        return ERR_BAD_HANDLE;

    mutex_acquire(&spi_ins->lock);
    ret = HAL_SPI_TransmitReceive(&spi_ins->handle, tx_buf, rx_buf, len, 1000);
    mutex_release(&spi_ins->lock);

    return ret;
}

status_t spi_transmit(int bus, uint8_t *tx_buf, uint16_t len) {
    uint8_t ret = HAL_OK;

    struct spi_instance *spi_ins = spi[bus];
    if (bus < 0 || bus > NUM_SPI || !spi_ins)
        return ERR_BAD_HANDLE;

    mutex_acquire(&spi_ins->lock);
    ret = HAL_SPI_Transmit(&spi_ins->handle, tx_buf, len, 1000);
    mutex_release(&spi_ins->lock);

    return ret;
}

status_t spi_receive(int bus, uint8_t *rx_buf, uint16_t len) {
    uint8_t ret = HAL_OK;

    struct spi_instance *spi_ins = spi[bus];
    if (bus < 0 || bus > NUM_SPI || !spi_ins)
        return ERR_BAD_HANDLE;

    mutex_acquire(&spi_ins->lock);
    ret = HAL_SPI_Receive(&spi_ins->handle, rx_buf, len, 1000);
    mutex_release(&spi_ins->lock);

    return ret;
}
