
/**
 * @brief
 * ############################### STANDBY MODE ###############################
 * Standby mode allows to acheive the lowest power consumption
 * it's based on the cortex-M4 with deepsleep mode, with the voltage regulator disabled
 * - The 1.2V domain is powered off.
 * - Core disabled
 * - PLL disabled
 * - HSI oscillator disabled
 * - HSE oscillator disabled
 * - SRAM is switched off
 * 
 * ############################### SLEEP MODE #################################
 * in sleep mode :
 * - The core is disabled
 * - All others perepherals keep running 
 * - All I/O pins keep the same state as the run mode
 * 
 * ############################### STOP MODE #################################
 * it's based on the cortex-M4 with deepsleep mode combined with perepheral clock gating
 * - The 1.8V domain is powered off.
 * - Core disabled
 * - PLL disabled
 * - HSI oscillator disabled
 * - HSE oscillator disabled
 * - SRAM and register contents are preserved
 * - All I/O pins keep the same state as the run mode

 * ############################### RUN MODE #################################
 * In run mode we can power dwn some perepherals such as ADC
 * Also we can slow down system clocks (SYSCLK, HCLK, PCLK) or switch to HSI oscillator

*/


#include <platform/lowPower.h>
#include <stdio.h>
#include "stm32f7xx_hal.h"
#include <stm32f7xx_hal_tim.h>
#include <stm32f7xx_hal_pwr.h>
#include <stm32f7xx_hal_pwr_ex.h>
#include <stm32f7xx_hal_gpio.h>
#include <stm32f7xx_hal_cortex.h>
#include <stm32f7xx.h>
#include <core_cm7.h>


static void standby_mode(void){

RCC->APB1ENR |= (1U << 28);     // enable PWR interface clock

SCB->SCR     |= (1U << 2);      // set the deepsleep bit in system controle block -> system control register

PWR->CR1     |= (1U << 1);      // enter standby mode

 // the wakeup flag bit is automatically cleared after 2 system cycles

PWR->CR1     |= (1U << 3);      // clear standby flag

// enable the wakeup pin ( PA0/wakeup 1) and forced input pull down
// for stm32f7 any event (rising edge or falling edge) on PA8 can wake up the core, depending on 
// WUPP1 bit in CR2 register 
// by reseting this bit the core willwake up after detecting a rising edge on PA0

PWR->CSR2    |= (1U << 8);      


#if defined ( __CC_ARM)
  __force_stores();
#endif

  __WFI();                       // Request Wait For Interrupt 


}

static bool verify_sleep_mode(void){
    // check the standby flag 
    if( (PWR->CSR1) & (1U << 1)){
        PWR->CR1 |= (1U << 3);    // clear standby flag
        
        return true;
    }
     
    return false;

}



 
static void stop_mode(){

HAL_Init();
__HAL_RCC_GPIOA_CLK_ENABLE();
__HAL_RCC_PWR_CLK_ENABLE();
__HAL_RCC_SYSCFG_CLK_ENABLE();
__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

GPIO_InitTypeDef GPIO_InitStruct={0};
GPIO_InitStruct.Pin=GPIO_PIN_0;
// configuration for the old izigoborad (with nc resistor)
GPIO_InitStruct.Mode=GPIO_MODE_IT_FALLING;
GPIO_InitStruct.Pull=GPIO_PULLUP;

/* for newer izigoboard
GPIO_InitStruct.Mode=GPIO_MODE_IT_RISING;
GPIO_InitStruct.Pull=GPIO_PULLDOWN;
*/
HAL_GPIO_Init(GPIOA,&GPIO_InitStruct);
HAL_NVIC_SetPriority(EXTI0_IRQn,0,0);
HAL_NVIC_EnableIRQ(EXTI0_IRQn);
HAL_GPIO_EXTI_IRQHandler(EXTI0_IRQn);

HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON,PWR_STOPENTRY_WFI);
printf("exit from wfi\n");


}

static void sleep_mode(){

  HAL_Init();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_RCC_SYSCFG_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  GPIO_InitTypeDef GPIO_InitStruct={0};
  GPIO_InitStruct.Pin=GPIO_PIN_0;
  GPIO_InitStruct.Mode=GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull=GPIO_PULLUP;

  HAL_GPIO_Init(GPIOA,&GPIO_InitStruct);
  HAL_NVIC_SetPriority(EXTI0_IRQn,0,0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
  HAL_GPIO_EXTI_IRQHandler(EXTI0_IRQn);

  SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk; // Disable systick interrupts  <-- problem here ( systick interrupt not disabled)
  HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON,PWR_SLEEPENTRY_WFI);
  SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;  // Resume systick interrupts
}

/**
 * 
 * @fn ADCx_power-off(void)
 * @brief this functions can be used in run mode
 *        to power down ADCs.
 *        by calling this function ADC consumes almost no power (only few µA)
 */


void ADC1_power_off(void){
  ADC1->CR2 &=~ (1U << 0);
}

void ADC2_power_off(void){
  ADC2->CR2 &=~ (1U << 0);
}

void ADC3_power_off(void){
  ADC3->CR2 &=~ (1U << 0);
}


/**
 * 
 * @fn ADCx_power-on(void)
 * @brief this functions can be used in run mode
 *        to power up ADCs.
 */

void ADC1_power_on(void){
  ADC1->CR2 |= (1U << 0);
}

void ADC2_power_on(void){
  ADC2->CR2 |= (1U << 0);
}

void ADC3_power_on(void){
  ADC3->CR2 |= (1U << 0);
}



void enter_standby_mode(){
    standby_mode();
}

void enter_sleep_mode(){
    sleep_mode();
}

void enter_stop_mode(){
    stop_mode();
} 

bool check_sleep_mode(){
    return verify_sleep_mode();
}
