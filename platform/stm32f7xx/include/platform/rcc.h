/*
 * Copyright (c) 2016 Erik Gilling
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT
 */
#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>

#include <stm32f7xx.h>

enum {
    STM32_RCC_REG_AHB1 = 0,
    STM32_RCC_REG_AHB2 = 1,
    STM32_RCC_REG_AHB3 = 2,
    STM32_RCC_REG_APB1 = 3,
    STM32_RCC_REG_APB2 = 4,
};

#define STM32_RCC_CLK(reg, index)  (((reg) << 16) | (index))
#define STM32_RCC_CLK_AHB1(index)  STM32_RCC_CLK(STM32_RCC_REG_AHB1, index)
#define STM32_RCC_CLK_AHB2(index)  STM32_RCC_CLK(STM32_RCC_REG_AHB2, index)
#define STM32_RCC_CLK_AHB3(index)  STM32_RCC_CLK(STM32_RCC_REG_AHB3, index)
#define STM32_RCC_CLK_APB1(index)  STM32_RCC_CLK(STM32_RCC_REG_APB1, index)
#define STM32_RCC_CLK_APB2(index)  STM32_RCC_CLK(STM32_RCC_REG_APB2, index)

#define STM32_RCC_CLK_REG(clk) ((clk) >> 16)
#define STM32_RCC_CLK_INDEX(clk) ((clk) & 0xffff)

typedef enum {

    // AHB1 clocks.
     
    STM32_RCC_CLK_IOPA      =  STM32_RCC_CLK_AHB1(0),
    STM32_RCC_CLK_IOPB      =  STM32_RCC_CLK_AHB1(1),
    STM32_RCC_CLK_IOPC      =  STM32_RCC_CLK_AHB1(2),
    STM32_RCC_CLK_IOPD      =  STM32_RCC_CLK_AHB1(3),
    STM32_RCC_CLK_IOPE      =  STM32_RCC_CLK_AHB1(4),
    STM32_RCC_CLK_IOPF      =  STM32_RCC_CLK_AHB1(5),
    STM32_RCC_CLK_IOPG      =  STM32_RCC_CLK_AHB1(6),
    STM32_RCC_CLK_IOPH      =  STM32_RCC_CLK_AHB1(7),
    STM32_RCC_CLK_IOPI      =  STM32_RCC_CLK_AHB1(8),
    STM32_RCC_CLK_IOPJ      =  STM32_RCC_CLK_AHB1(9),
    STM32_RCC_CLK_IOPK      =  STM32_RCC_CLK_AHB1(10),
    STM32_RCC_CLK_CRC       =  STM32_RCC_CLK_AHB1(12),
    STM32_RCC_CLKBKPSRAM    =  STM32_RCC_CLK_AHB1(18),
    STM32_RCC_CLK_DTCMRAM   =  STM32_RCC_CLK_AHB1(20),
    STM32_RCC_CLK_DMA1      =  STM32_RCC_CLK_AHB1(21),
    STM32_RCC_CLK_DMA2      =  STM32_RCC_CLK_AHB1(22),
    STM32_RCC_CLK_DMA2D     =  STM32_RCC_CLK_AHB1(23),
    STM32_RCC_CLK_ETHMAC    =  STM32_RCC_CLK_AHB1(25),
    STM32_RCC_CLK_ETHMACTX  =  STM32_RCC_CLK_AHB1(26),
    STM32_RCC_CLK_ETHMACRX  =  STM32_RCC_CLK_AHB1(27),
    STM32_RCC_CLK_ETHMACPTP =  STM32_RCC_CLK_AHB1(28),
    STM32_RCC_CLK_OTGHS     =  STM32_RCC_CLK_AHB1(29),
    STM32_RCC_CLK_OTGHSULPI =  STM32_RCC_CLK_AHB1(30),
    
    

    // AHB2 clocks.

    STM32_RCC_CLK_DCMI      =  STM32_RCC_CLK_AHB2(0),
    STM32_RCC_CLK_JPEG      =  STM32_RCC_CLK_AHB2(1),
    STM32_RCC_CLK_CRYP      =  STM32_RCC_CLK_AHB2(4),
    STM32_RCC_CLK_HASH      =  STM32_RCC_CLK_AHB2(5),
    STM32_RCC_CLK_RNG       =  STM32_RCC_CLK_AHB2(6),
    STM32_RCC_CLK_OTGFS     =  STM32_RCC_CLK_AHB2(7),

    // AHB3 clocks.

    STM32_RCC_CLK_FMC       =  STM32_RCC_CLK_AHB3(0),
    STM32_RCC_CLK_QSPI      =  STM32_RCC_CLK_AHB3(1),


    // APB1 clocks.

    STM32_RCC_CLK_TIM2    =  STM32_RCC_CLK_APB1(0),
    STM32_RCC_CLK_TIM3    =  STM32_RCC_CLK_APB1(1),
    STM32_RCC_CLK_TIM4    =  STM32_RCC_CLK_APB1(2),
    STM32_RCC_CLK_TIM5    =  STM32_RCC_CLK_APB1(3),
    STM32_RCC_CLK_TIM6    =  STM32_RCC_CLK_APB1(4),
    STM32_RCC_CLK_TIM7    =  STM32_RCC_CLK_APB1(5),
    STM32_RCC_CLK_TIM12   =  STM32_RCC_CLK_APB1(6),
    STM32_RCC_CLK_TIM13   =  STM32_RCC_CLK_APB1(7),
    STM32_RCC_CLK_TIM14   =  STM32_RCC_CLK_APB1(8),
    STM32_RCC_CLK_LPTIM1  =  STM32_RCC_CLK_APB1(9),
    STM32_RCC_CLK_RTCAPB  =  STM32_RCC_CLK_APB1(10),
    STM32_RCC_CLK_WWDG    =  STM32_RCC_CLK_APB1(11),
    STM32_RCC_CLK_CAN3    =  STM32_RCC_CLK_APB1(13),
    STM32_RCC_CLK_SPI2    =  STM32_RCC_CLK_APB1(14),
    STM32_RCC_CLK_SPI3    =  STM32_RCC_CLK_APB1(15),
    STM32_RCC_CLK_SPDIFRX =  STM32_RCC_CLK_APB1(16),
    STM32_RCC_CLK_USART2  =  STM32_RCC_CLK_APB1(17),
    STM32_RCC_CLK_USART3  =  STM32_RCC_CLK_APB1(18),
    STM32_RCC_CLK_USART4  =  STM32_RCC_CLK_APB1(19),
    STM32_RCC_CLK_USART5  =  STM32_RCC_CLK_APB1(20),
    STM32_RCC_CLK_I2C1    =  STM32_RCC_CLK_APB1(21),
    STM32_RCC_CLK_I2C2    =  STM32_RCC_CLK_APB1(22),
    STM32_RCC_CLK_I2C3    =  STM32_RCC_CLK_APB1(23),
    STM32_RCC_CLK_I2C4    =  STM32_RCC_CLK_APB1(24),
    STM32_RCC_CLK_CAN1    =  STM32_RCC_CLK_APB1(25),
    STM32_RCC_CLK_CAN2    =  STM32_RCC_CLK_APB1(26),
    STM32_RCC_CLK_CEC     =  STM32_RCC_CLK_APB1(27),
    STM32_RCC_CLK_PWR     =  STM32_RCC_CLK_APB1(28),
    STM32_RCC_CLK_DAC     =  STM32_RCC_CLK_APB1(29),
    STM32_RCC_CLK_USART7  =  STM32_RCC_CLK_APB1(30),
    STM32_RCC_CLK_USART8  =  STM32_RCC_CLK_APB1(31),


    // APB2 clocks.

    STM32_RCC_CLK_TIM1    =  STM32_RCC_CLK_APB2(0),
    STM32_RCC_CLK_TIM8    =  STM32_RCC_CLK_APB2(1),
    STM32_RCC_CLK_USART1  =  STM32_RCC_CLK_APB2(4),
    STM32_RCC_CLK_USART6  =  STM32_RCC_CLK_APB2(5),
    STM32_RCC_CLK_SDMMC2  =  STM32_RCC_CLK_APB2(7),
    STM32_RCC_CLK_ADC1    =  STM32_RCC_CLK_APB2(8),
    STM32_RCC_CLK_ADC2    =  STM32_RCC_CLK_APB2(9),
    STM32_RCC_CLK_ADC3    =  STM32_RCC_CLK_APB2(10),
    STM32_RCC_CLK_SDMMC1  =  STM32_RCC_CLK_APB2(11),
    STM32_RCC_CLK_SPI1    =  STM32_RCC_CLK_APB2(12),
    STM32_RCC_CLK_SPI4    =  STM32_RCC_CLK_APB2(13),
    STM32_RCC_CLK_SYSCFG  =  STM32_RCC_CLK_APB2(14),
    STM32_RCC_CLK_TIM9    =  STM32_RCC_CLK_APB2(16),
    STM32_RCC_CLK_TIM10   =  STM32_RCC_CLK_APB2(17),
    STM32_RCC_CLK_TIM11   =  STM32_RCC_CLK_APB2(18),
    STM32_RCC_CLK_SPI5    =  STM32_RCC_CLK_APB2(20),
    STM32_RCC_CLK_SPI6    =  STM32_RCC_CLK_APB2(21),
    STM32_RCC_CLK_SAI1    =  STM32_RCC_CLK_APB2(22),
    STM32_RCC_CLK_SAI2    =  STM32_RCC_CLK_APB2(23),
    STM32_RCC_CLK_LTDC    =  STM32_RCC_CLK_APB2(26),
    STM32_RCC_CLK_DSI     =  STM32_RCC_CLK_APB2(27),
    STM32_RCC_CLK_DFSDM1  =  STM32_RCC_CLK_APB2(29),
    STM32_RCC_CLK_MDIO    =  STM32_RCC_CLK_APB2(30),


} stm32_rcc_clk_t;

