#include <lk/err.h>
#include <kernel/spinlock.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
// include library depending on the 
#include <stm32f7xx.h>

typedef struct stm32_timer_config_ stm32_timer_config_t;


// function to call in other c files:
int* get_used_channels(int timer);
void pwm_init_chan(int timer,int chan);
void set_pwm_frequency(int timer,int f );
void set_timer_resolution(int timer, int resolution);
void set_duty_cycle(int timer, int chan, int duty_cycle);
void pwm_start(int timer);
void set_channel(int timer, int chan, int mode);
void set_type(int timer,int type);
