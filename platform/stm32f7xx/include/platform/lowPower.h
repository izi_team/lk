// impliment standby mode as low power mode


#include <arch/arm/cm.h>
#include <platform/rcc.h>
#include <stm32f7xx.h>
#include <stdbool.h>


bool check_sleep_mode(void);
void enter_standby_mode(void);
void enter_sleep_mode(void);
void enter_stop_mode(void);
void ADC1_power_off(void);
void ADC2_power_off(void);
void ADC3_power_off(void);
void ADC1_power_on(void);
void ADC2_power_on(void);
void ADC3_power_on(void);
