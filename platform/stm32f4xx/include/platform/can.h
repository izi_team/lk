#include <stm32f4xx_hal_can.h>
#include <stm32f4xx_hal.h>
#include <stm32f4xx_hal_rcc.h>
#include <stm32f4xx_hal_rcc_ex.h>
#include <stm32f4xx_hal_gpio.h>
#include <stm32f4xx_hal_cortex.h>

/**
 * A brief discreption of functions bellow are detailed in source file can.c
 */

void can_init(void);                                                                
void can_stop(void);
void can_config_filter(uint32_t mask_id, uint32_t filter_id, uint8_t filter_bank);
void can_transmit_std_frame(uint16_t node_id,char data[]);
void can_transmit_ext_frame(uint32_t node_id,char data[]);
char* can_receive_frame_it(void);
char* can_receive_frame(void);
