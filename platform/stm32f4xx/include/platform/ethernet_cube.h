#include <stm32f4xx_hal_eth.h>
#include <stm32f4xx_hal.h>
#include <stm32f4xx_hal_rcc.h>
#include <stm32f4xx_hal_rcc_ex.h>
#include <stm32f4xx_hal_gpio.h>
#include <stm32f4xx_hal_cortex.h>

#include "LWIP/lwip.h"

void ethernet_start(void);
void ethernet_ping(void);