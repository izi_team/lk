/*
 * Copyright (c) 2019 Mihail CHERCIU mihail.cherciu@izitron.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef __PLATFORM_STM32_ADC_H
#define __PLATFORM_STM32_ADC_H

#define VREFINT_CAL_ADDR    ((uint16_t*) (0x1FFF7A2AU))
#define VREFINT_CAL_VREF    (3.3)

typedef enum {
    ADC_0 = 0,
    ADC_1,
    ADC_2,
    ADC_3,
    ADC_4,
    ADC_5,
    ADC_6,
    ADC_7,
    ADC_8,
    ADC_9,
    ADC_10,
    ADC_11,
    ADC_12,
    ADC_13,
    ADC_14,
    ADC_15,
    ADC_VREFINT = 17,
    MAX_ADC
} adc_channel_t;

void adc_init_early(void);
void adc_init(void);
uint16_t get_adc_value(adc_channel_t adc_channel);
void get_adc_buffer(adc_channel_t adc_channel, uint16_t *buf, uint16_t length);
void adc_start(void);
void adc_stop(void);

#endif  // __PLATFORM_STM32_ADC_H
