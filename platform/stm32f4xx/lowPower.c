
/**
 * @brief
 * ############################### STANDBY MODE ###############################
 * Standby mode allows to acheive the lowest power consumption
 * it's based on the cortex-M4 with deepsleep mode, with the voltage regulator disabled
 * - The 1.2V domain is powered off.
 * - Core disabled
 * - PLL disabled
 * - HSI oscillator disabled
 * - HSE oscillator disabled
 * - SRAM is switched off
 * 
 * ############################### SLEEP MODE #################################
 * in sleep mode :
 * - The core is disabled
 * - All others perepherals keep running 
 * - All I/O pins keep the same state as the run mode
 * 
 * ############################### STOP MODE #################################
 * it's based on the cortex-M4 with deepsleep mode combined with perepheral clock gating
 * - The 1.8V domain is powered off.
 * - Core disabled
 * - PLL disabled
 * - HSI oscillator disabled
 * - HSE oscillator disabled
 * - SRAM and register contents are preserved
 * - All I/O pins keep the same state as the run mode

 * ############################### RUN MODE #################################
 * In run mode we can power dwn some perepherals such as ADC
 * Also we can slow down system clocks (SYSCLK, HCLK, PCLK) or switch to HSI oscillator

*/


#include <platform/lowPower.h>
#include "stm32f4xx_hal.h"
#include <stm32f4xx_hal_tim.h>
#include <stm32f4xx_hal_pwr.h>
#include <stm32f4xx_hal_gpio.h>
#include <stm32f4xx_hal_cortex.h>
#include <stdio.h>
#include <core_cm4.h>


static void standby_mode(void){

RCC->APB1ENR |= (1U << 28);     // enable PWR interface clock

SCB->SCR     |= (1U << 2);      // set the deepsleep bit in system controle block -> system control register

PWR->CR      |= (1U << 1);      // enter standby mode

PWR->CR      |= (1U << 2);      // clear the wakeup flag bit

PWR->CSR     |= (1U << 8);      // enable the wakeup pin ( PA0/wakeup 1) and forced input pull down


#if defined ( __CC_ARM)
  __force_stores();
#endif

  __WFI();                       // Request Wait For Interrupt 


}




/*     sleep mode with low level programming */
/*
static void sleep_mode(void){

RCC->APB1ENR |= (1U << 28);     // enable PWR interface clock

// configure the PA0 as external interrupt with pull-up  

//enable the SYSCFG/AFIO bit

RCC->APB2ENR |= (1<<14);

// configure PA0 input
RCC->AHB1ENR |=  (1 << 0 );
GPIOA->MODER &=~ (0x3 << 0);
GPIOA->PUPDR |=  (1 << 0);
GPIOA->PUPDR &=~ (1 << 1);

// configure PA0 as  external interrupt

SYSCFG->EXTICR[0] &=~ (0xf <<0);

// disable the interrupt mask
EXTI->IMR |= (1<<0);

// configure interrupt on falling edge

EXTI->RTSR &=~ (1 << 0);
EXTI->FTSR |=  (1 << 0);
// reset the deepsleep bit in system controle block -> system control register
SCB->SCR     &= ~(1U << 2);      
#if defined ( __CC_ARM)
  __force_stores();
#endif
SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk; // Disable systick interrupts  <-- problem here ( systick interrupt not disabled)
  __WFI();                                  // Request Wait For Interrupt 
SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;  // Resume systick interrupts
printf("exit from wfi\n");

}
*/

 /* Same function but with HAL API */
 
static void stop_mode(){

HAL_Init();
__HAL_RCC_GPIOA_CLK_ENABLE();
__HAL_RCC_PWR_CLK_ENABLE();
__HAL_RCC_SYSCFG_CLK_ENABLE();
__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

GPIO_InitTypeDef GPIO_InitStruct={0};
GPIO_InitStruct.Pin=GPIO_PIN_0;
// configuration for the old izigoborad (with nc resistor)
GPIO_InitStruct.Mode=GPIO_MODE_IT_FALLING;
GPIO_InitStruct.Pull=GPIO_PULLUP;

/* for newer izigoboard
GPIO_InitStruct.Mode=GPIO_MODE_IT_RISING;
GPIO_InitStruct.Pull=GPIO_PULLDOWN;
*/
HAL_GPIO_Init(GPIOA,&GPIO_InitStruct);
HAL_NVIC_SetPriority(EXTI0_IRQn,3,0);
HAL_NVIC_EnableIRQ(EXTI0_IRQn);
HAL_GPIO_EXTI_IRQHandler(EXTI0_IRQn);

HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON,PWR_STOPENTRY_WFI);
printf("exit from wfi\n");


}

static void sleep_mode(){

  HAL_Init();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_RCC_SYSCFG_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  GPIO_InitTypeDef GPIO_InitStruct={0};
  GPIO_InitStruct.Pin=GPIO_PIN_0;
  GPIO_InitStruct.Mode=GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull=GPIO_PULLUP;

  HAL_GPIO_Init(GPIOA,&GPIO_InitStruct);
  HAL_NVIC_SetPriority(EXTI0_IRQn,3,0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
  //HAL_GPIO_EXTI_IRQHandler(EXTI0_IRQn);

  SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk; // Disable systick interrupts  <-- problem here ( systick interrupt not disabled)
  HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON,PWR_SLEEPENTRY_WFI);
  SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;  // Resume systick interrupts
}

/**
 * 
 * @fn ADCx_power-off(void)
 * @brief this functions can be used in run mode
 *        to power down ADCs.
 *        by calling this function ADC consumes almost no power (only few µA)
 */


void ADC1_power_off(void){
  ADC1->CR2 &=~ (1U << 0);
}

void ADC2_power_off(void){
  ADC2->CR2 &=~ (1U << 0);
}

void ADC3_power_off(void){
  ADC3->CR2 &=~ (1U << 0);
}


/**
 * 
 * @fn ADCx_power-on(void)
 * @brief this functions can be used in run mode
 *        to power up ADCs.
 */

void ADC1_power_on(void){
  ADC1->CR2 |= (1U << 0);
}

void ADC2_power_on(void){
  ADC2->CR2 |= (1U << 0);
}

void ADC3_power_on(void){
  ADC3->CR2 |= (1U << 0);
}


static bool verify_sleep_mode(void){
    // check the standby flag 
    if( (PWR->CSR) & (1U << 1)){
        PWR->CR |= (1U << 3);    // clear standby flag
        PWR->CR |= (1U << 2);    // clear the wakeup flag bit
        return true;
    }
     
    return false;

}




void enter_standby_mode(){
    standby_mode();
}

void enter_sleep_mode(){
    sleep_mode();
}

void enter_stop_mode(){
    stop_mode();
} 

bool check_sleep_mode(){
    return verify_sleep_mode();
}
