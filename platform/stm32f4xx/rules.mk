LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

# ROMBASE, MEMBASE, and MEMSIZE are required for the linker script
ROMBASE ?= 0x08000000
MEMBASE ?= 0x20000000

ARCH := arm
ARM_CPU := cortex-m4

ifeq ($(STM32_CHIP),stm32f407)
GLOBAL_DEFINES += STM32F407
MEMSIZE ?= 131072
ROMSIZE ?= 524288
FOUND_CHIP := true
endif
ifeq ($(STM32_CHIP),stm32f417)
GLOBAL_DEFINES += STM32F417
MEMSIZE ?= 131072
ROMSIZE ?= 524288
FOUND_CHIP := true
endif
ifeq ($(STM32_CHIP),stm32f429)
GLOBAL_DEFINES += STM32F429
MEMSIZE ?= 524288
ROMSIZE ?= 1048576
FOUND_CHIP := true
endif

ifeq ($(FOUND_CHIP),)
$(error unknown STM32F4xx chip $(STM32_CHIP))
endif

GLOBAL_DEFINES += \
	MEMSIZE=$(MEMSIZE) \
	ROMSIZE=$(ROMSIZE) \
	STM32_CHIP=\"$(STM32_CHIP)\"

MODULE_SRCS += \
	$(LOCAL_DIR)/vectab.c \
	$(LOCAL_DIR)/init.c \
	$(LOCAL_DIR)/adc.c \
	$(LOCAL_DIR)/i2c.c \
	$(LOCAL_DIR)/can.c \
	$(LOCAL_DIR)/lowPower.c \
	$(LOCAL_DIR)/timer_pwm.c \
	$(LOCAL_DIR)/spi.c \
	$(LOCAL_DIR)/exti.c

#	$(LOCAL_DIR)/flash.c

# use a two segment memory layout, where all of the read-only sections
# of the binary reside in rom, and the read/write are in memory. The
# ROMBASE, MEMBASE, and MEMSIZE make variables are required to be set
# for the linker script to be generated properly.
#
LINKER_SCRIPT += \
	$(BUILDDIR)/system-twosegment.ld

MODULE_DEPS += \
	platform/stm32f4xx/CMSIS \
	platform/stm32f4xx/STM32F4xx_HAL_Driver \
	platform/stm32 \
	arch/arm/arm-m/systick \
	lib/cbuf \
	lib/bio

include make/module.mk
