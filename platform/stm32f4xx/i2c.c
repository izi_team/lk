/*
 * Copyright (c) 2017 The Fuchsia Authors
 * Copyright (c) 2020 IZITRON (Mihail CHERCIU)
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT
 */

#include <dev/i2c.h>
#include <lk/err.h>
#include <kernel/mutex.h>
#include <platform/platform_cm.h>

#define NUM_I2C 3

struct i2c_instance {
    I2C_HandleTypeDef handle;
    mutex_t lock;
};

#if ENABLE_I2C1
static struct i2c_instance i2c1;
#endif

#if ENABLE_I2C3
static struct i2c_instance i2c3;
#endif

static struct i2c_instance *const i2c[NUM_I2C + 1] = {
#if ENABLE_I2C1
    [1] = &i2c1,
#endif
#if ENABLE_I2C3
    [3] = &i2c3,
#endif
};

/* This function is called by HAL_I2C_Init() */
void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c) {
    switch ((uintptr_t)hi2c->Instance) {
        case (uintptr_t)I2C1:
            __HAL_RCC_I2C1_CLK_ENABLE();
            HAL_NVIC_SetPriority(I2C1_EV_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);
            break;
        case (uintptr_t)I2C3:
            __HAL_RCC_I2C3_CLK_ENABLE();
            HAL_NVIC_SetPriority(I2C3_EV_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(I2C3_EV_IRQn);
            break;
        default:
            panic("unimplemented i2c\n");
    }
}

static void stm32_i2c_early_init(struct i2c_instance *i2c_ins, I2C_TypeDef *i2c, int speed)
{
    i2c_ins->handle.Instance = i2c;
    i2c_ins->handle.Init.ClockSpeed = speed;
    i2c_ins->handle.Init.DutyCycle = I2C_DUTYCYCLE_2;
    i2c_ins->handle.Init.OwnAddress1 = 0;
    i2c_ins->handle.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    i2c_ins->handle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    i2c_ins->handle.Init.OwnAddress2 = 0;
    i2c_ins->handle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    i2c_ins->handle.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;

    if (HAL_I2C_Init(&i2c_ins->handle) != HAL_OK)
    {
        Error_Handler();
    }

    mutex_init(&i2c_ins->lock);
}

void i2c_init(void) {

}

void i2c_init_early(void) {
#ifdef ENABLE_I2C1
    stm32_i2c_early_init(i2c[1], I2C1, 100000);
#endif

#ifdef ENABLE_I2C3
    stm32_i2c_early_init(i2c[3], I2C3, 100000);
#endif
}

#ifdef ENABLE_I2C1
void stm32_I2C1_EV_IRQ(void) {
    HAL_I2C_EV_IRQHandler(&i2c1.handle);
}

void stm32_I2C1_ER_IRQ(void) {
    HAL_I2C_ER_IRQHandler(&i2c1.handle);
}
#endif

#ifdef ENABLE_I2C3
void stm32_I2C3_EV_IRQ(void) {
    HAL_I2C_EV_IRQHandler(&i2c3.handle);
}

void stm32_I2C3_ER_IRQ(void) {
    HAL_I2C_ER_IRQHandler(&i2c3.handle);
}
#endif

status_t i2c_transmit(int bus, uint8_t address, const void *buf, size_t count) {
    uint8_t ret = HAL_OK;

    struct i2c_instance *i2c_ins = i2c[bus];
    if (bus < 0 || bus > NUM_I2C || !i2c_ins)
        return ERR_BAD_HANDLE;

    mutex_acquire(&i2c_ins->lock);
    ret = HAL_I2C_Master_Transmit(&i2c_ins->handle, address, buf, count, 100);
    mutex_release(&i2c_ins->lock);

    return ret;
}

status_t i2c_receive(int bus, uint8_t address, void *buf, size_t count) {
    uint8_t ret = HAL_OK;

    struct i2c_instance *i2c_ins = i2c[bus];
    if (bus < 0 || bus > NUM_I2C || !i2c_ins)
        return ERR_BAD_HANDLE;

    mutex_acquire(&i2c_ins->lock);
    ret = HAL_I2C_Master_Receive(&i2c_ins->handle, address, buf, count, 100);
    mutex_release(&i2c_ins->lock);

    return ret;
}
