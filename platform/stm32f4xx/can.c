#include <platform/can.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>


CAN_FilterTypeDef canfilterconfig;      // filter configuration 

CAN_TxHeaderTypeDef TxHeader;           // used to store header information ( RTR DLC..)
CAN_RxHeaderTypeDef RxHeader;           // used to store header information ( RTR DLC..)

uint8_t  TxData[8];                     // Data feild (0-8 bytes)
uint32_t TxMailbox;                     // mailbox wich will be sent to the CAN bus

uint8_t  RxData[8]={' '};

CAN_HandleTypeDef hcan1;


/*************** Error handler function ****************/
static void Error_Handler(void){
    __disable_irq();
    printf("error!\n");
}




/*****      STANDARD CAN 2.0A : 11 bits identifier **************/

/**
 * @fn void std_can_header(uint16_t node_id,char data[])
 * @param node_id the identifier of the can frame
 * @param data    the message entended to transmit
 * @brief this function prepare the can frame to transmit
 */


static void std_can_header(uint16_t node_id,char data[]){

    TxHeader.IDE     = CAN_ID_STD;              // standard identifier
    TxHeader.StdId   = node_id;                 // can node identifier 
    TxHeader.RTR     = CAN_RTR_DATA;            // can frame type : data frame 
    for(int i=0;i<8;i++)
        TxData[i]=data[i];
}

/**
 * @fn void send_std_frame(uint16_t node_id,char data[])
 * @param node_id the identifier of the can frame
 * @param data    the message entended to transmit
 * @brief this function transmit the can frame
 */

static void send_std_frame(uint16_t node_id,char data[]){
    
    /*int data_size=sizeof(data);
    printf("data_size=%d\n",data_size);
    if(data_size >8){
        printf("maximum 8 bytes given:%d",data_size);
    }
    else{}*/

    std_can_header(node_id,data);
    TxHeader.DLC=8;
    HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData,&TxMailbox);
    while(HAL_CAN_IsTxMessagePending(&hcan1,TxMailbox));             // wait until the transmission is complete
    
}


/*****      EXTENDED CAN 2.0B  : 29 bits identifier **************/

/**
 * @fn void std_can_header(uint16_t node_id,char data[])
 * @param node_id the identifier of the can frame
 * @param data    the message entended to transmit
 * @brief this function prepare the can frame to transmit
 */

static void ext_can_header(uint32_t node_id,char data[]){

    TxHeader.IDE     = CAN_ID_EXT;                         // extended identifier
    TxHeader.StdId   = ((node_id & 0xFFF00000) >> 18);     // can node identifier 
    TxHeader.ExtId   = node_id & 0xFFFFF;
    TxHeader.RTR     = CAN_RTR_DATA;                      // can frame type : data frame 
    for(int i=0;i<8;i++)
        TxData[i]=data[i];
}


/**
 * @fn void send_std_frame(uint16_t node_id,char data[])
 * @param node_id the identifier of the can frame
 * @param data    the message entended to transmit
 * @brief this function transmit the can frame
 */

static void send_ext_frame(uint32_t node_id,char data[]){

    std_can_header(node_id,data);
    TxHeader.DLC=8;
    HAL_CAN_AddTxMessage(&hcan1, &TxHeader, data, &TxMailbox);
    while(HAL_CAN_IsTxMessagePending(&hcan1,TxMailbox));    // wait until the transmission is complete
    
}



/**
 * @fn void can_initialize(void)
 * @brief initialize the CAN1 perepheral by setting the can mode and 
 *        baude rate at 500 Kbit/s with APB2 frequency up to 48 MHz
 */

static void can_initialize(void){
    hcan1.Instance=CAN1;
    hcan1.Init.Prescaler=16;
    hcan1.Init.Mode=CAN_MODE_NORMAL;
    hcan1.Init.SyncJumpWidth=CAN_SJW_1TQ;
    hcan1.Init.TimeSeg1 = CAN_BS1_1TQ;
    hcan1.Init.TimeSeg2 = CAN_BS2_1TQ;
    hcan1.Init.TimeTriggeredMode = DISABLE;
    hcan1.Init.AutoBusOff = DISABLE;
    hcan1.Init.AutoWakeUp = DISABLE;
    hcan1.Init.AutoRetransmission= DISABLE;
    hcan1.Init.ReceiveFifoLocked = DISABLE;
    hcan1.Init.TransmitFifoPriority = DISABLE;
    if(HAL_CAN_Init(&hcan1) != HAL_OK)
        Error_Handler();


}

/**
 * @fn void CAN1_MspInit(void)
 * @brief initialize the CAN1 hardware by activating the correspending clock
 *        and configure the CAN1_RX and CAN_TX pins
 */

static void CAN1_MspInit(void){
    __HAL_RCC_SYSCFG_CLK_ENABLE();
    __HAL_RCC_PWR_CLK_ENABLE();
    // perepheral clock enable 
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_CAN1_CLK_ENABLE();

    // configure PA11 and PA12
    // PA11 -------> CAN1_RX
    // PA12 -------> CAN1_TX
    GPIO_InitTypeDef GPIO_InitStruct ={0};

    GPIO_InitStruct.Pin          = GPIO_PIN_11 | GPIO_PIN_12;
    GPIO_InitStruct.Mode         = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull         = GPIO_NOPULL;
    GPIO_InitStruct.Speed        = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate    = GPIO_AF9_CAN1;
  
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    //CAN1 RX interrupt Init
    
    HAL_NVIC_SetPriority(CAN1_RX0_IRQn,0,0);
    HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);

}




/********************** Identifier Filtering *******************/

/**
 * @fn configure_filter(uint32_t mask_id, uint32_t filter_id, uint8_t filter_bank)
 * @param mask_id mask identifier
 * @param filter_id filter identifier
 * @param filter_bank the number of filter bank (should be between 0 and 28 )
 * @brief In order to reduce CPU load to filter out messages, this function
 *        set hardware filters and masks to get the wanted message
 */

void configure_filter(uint32_t mask_id, uint32_t filter_id, uint8_t filter_bank){

    canfilterconfig.FilterActivation     = CAN_FILTER_ENABLE;
    canfilterconfig.FilterBank           = filter_bank;
    canfilterconfig.FilterFIFOAssignment = CAN_FILTER_FIFO0;
    canfilterconfig.FilterIdHigh         = (filter_id >> 13 ) &0xFFFF; // STID[10:0] & EXTID[17:13]
    canfilterconfig.FilterIdLow          = (filter_id << 3 ) & 0xFFF8; // EXID[12:5] & 3 reserved
    canfilterconfig.FilterMaskIdHigh     = (mask_id >> 13) & 0xFFFF;
    canfilterconfig.FilterMaskIdLow      = (mask_id <<  3) & 0xFFF8;
    canfilterconfig.FilterMode           = CAN_FILTERMODE_IDMASK;
    canfilterconfig.FilterScale          = CAN_FILTERSCALE_32BIT;
    canfilterconfig.SlaveStartFilterBank = 28;                          // assign all filters to CAN1 sibce CAN2 will never be used

    HAL_CAN_ConfigFilter(&hcan1, &canfilterconfig);

}


/**
 * @fn void receive_frame_it(void)
 * @param hcan van1 handler 
 * @brief Receive CAN frame with interrupts on RX0
 */


void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan){

    if(HAL_CAN_GetRxMessage(hcan,CAN_RX_FIFO0, &RxHeader, RxData) != HAL_OK)
        Error_Handler();


}


static char* receive_frame_it(void){
    
    if(HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)
        Error_Handler();

    HAL_CAN_GetRxMessage(&hcan1,CAN_RX_FIFO0, &RxHeader, RxData);
    return RxData;
}


/**
 * @fn void receive_frame(void)
 * @param hcan van1 handler 
 * @brief Receive CAN frame 
 */


static char* receive_frame(void){

    HAL_CAN_GetRxMessage(&hcan1,CAN_RX_FIFO0, &RxHeader, RxData);
    return RxData;
}



void can_init(){
    HAL_Init();
    CAN1_MspInit();
    can_initialize();
    configure_filter(0x00000000,0x00000000,0);
    HAL_CAN_Start(&hcan1);
}

void can_stop(){
    HAL_CAN_Stop(&hcan1);
}
void can_config_filter(uint32_t mask_id, uint32_t filter_id, uint8_t filter_bank){
    configure_filter(mask_id,filter_id, filter_bank);
}

void can_transmit_std_frame(uint16_t node_id,char data[]){
    send_std_frame(node_id,data);
    memset(data,0,8);
    
}

void can_transmit_ext_frame(uint32_t node_id,char data[]){
    send_ext_frame( node_id, data);
}

char* can_receive_frame_it(void){
    return receive_frame_it();
}

char* can_receive_frame(void){
    return receive_frame();
}