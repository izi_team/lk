/*
 * Copyright (c) 2017 The Fuchsia Authors.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT
 */

#include <arch/arm/cm.h>
#include <lk/err.h>
#include <lk/trace.h>
#include <kernel/mutex.h>
#include <platform/exti.h>
#include <platform/platform_cm.h>

#define LOCAL_TRACE 1

bool __WEAK stm32_exti0_irq(void) {
    return false;
}

bool __WEAK stm32_exti1_irq(void) {
    return false;
}

bool __WEAK stm32_exti2_irq(void) {
    return false;
}

bool __WEAK stm32_exti3_irq(void) {
    return false;
}

bool __WEAK stm32_exti4_irq(void) {
    return false;
}

bool __WEAK stm32_exti5_irq(void) {
    return false;
}

bool __WEAK stm32_exti6_irq(void) {
    return false;
}

bool __WEAK stm32_exti7_irq(void) {
    return false;
}

bool __WEAK stm32_exti8_irq(void) {
    return false;
}

bool __WEAK stm32_exti9_irq(void) {
    return false;
}

bool __WEAK stm32_exti10_irq(void) {
    return false;
}

bool __WEAK stm32_exti11_irq(void) {
    return false;
}

bool __WEAK stm32_exti12_irq(void) {
    return false;
}

bool __WEAK stm32_exti13_irq(void) {
    return false;
}

bool __WEAK stm32_exti14_irq(void) {
    return false;
}

bool __WEAK stm32_exti15_irq(void) {
    return false;
}

#define STM32_DISPATCH_EXTI(pr, irq, resched) do { \
    if ((pr) & (1 << (irq))) { \
        resched |= stm32_exti ## irq ## _irq(); \
    } \
 } while(0)

#ifdef ENABLE_EXTI0
void stm32_EXTI0_IRQ(void) {
    arm_cm_irq_entry();

    uint32_t pr = EXTI->PR & 0x01;
    EXTI->PR = pr;

    bool resched = false;
    STM32_DISPATCH_EXTI(pr, 0, resched);

    arm_cm_irq_exit(resched);
}
#endif

#ifdef ENABLE_EXTI1
void stm32_EXTI1_IRQ(void) {
    arm_cm_irq_entry();

    uint32_t pr = EXTI->PR & 0x02;
    EXTI->PR = pr;

    bool resched = false;
    STM32_DISPATCH_EXTI(pr, 1, resched);

    arm_cm_irq_exit(resched);
}
#endif

#ifdef ENABLE_EXTI2
void stm32_EXTI2_IRQ(void) {
    arm_cm_irq_entry();

    uint32_t pr = EXTI->PR & 0x04;
    EXTI->PR = pr;

    bool resched = false;
    STM32_DISPATCH_EXTI(pr, 2, resched);

    arm_cm_irq_exit(resched);
}
#endif

#ifdef ENABLE_EXTI3
void stm32_EXTI3_IRQ(void) {
    arm_cm_irq_entry();

    uint32_t pr = EXTI->PR & 0x08;
    EXTI->PR = pr;

    bool resched = false;
    STM32_DISPATCH_EXTI(pr, 3, resched);

    arm_cm_irq_exit(resched);
}
#endif

#ifdef ENABLE_EXTI4
void stm32_EXTI4_IRQ(void) {
    arm_cm_irq_entry();

    uint32_t pr = EXTI->PR & 0x16;
    EXTI->PR = pr;

    bool resched = false;
    STM32_DISPATCH_EXTI(pr, 4, resched);

    arm_cm_irq_exit(resched);
}
#endif

#ifdef ENABLE_EXTI9_5
void stm32_EXTI9_5_IRQ(void) {
    arm_cm_irq_entry();

    uint32_t pr = EXTI->PR & 0x03f0U;
    EXTI->PR = pr;

    bool resched = false;
    STM32_DISPATCH_EXTI(pr, 5, resched);
    STM32_DISPATCH_EXTI(pr, 6, resched);
    STM32_DISPATCH_EXTI(pr, 7, resched);
    STM32_DISPATCH_EXTI(pr, 8, resched);
    STM32_DISPATCH_EXTI(pr, 9, resched);

    arm_cm_irq_exit(resched);
}
#endif

#ifdef ENABLE_EXTI15_10
void stm32_EXTI15_10_IRQ(void) {
    arm_cm_irq_entry();

    uint32_t pr = EXTI->PR & 0xfc00U;
    EXTI->PR = pr;

    bool resched = false;
    STM32_DISPATCH_EXTI(pr, 10, resched);
    STM32_DISPATCH_EXTI(pr, 11, resched);
    STM32_DISPATCH_EXTI(pr, 12, resched);
    STM32_DISPATCH_EXTI(pr, 13, resched);
    STM32_DISPATCH_EXTI(pr, 14, resched);
    STM32_DISPATCH_EXTI(pr, 15, resched);

    arm_cm_irq_exit(resched);
}
#endif


#undef STM32_DISPATCH_EXTI

static void stm32_exti_init_early(void)
{
    #ifdef ENABLE_EXTI0
        HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(EXTI0_IRQn);
    #endif

    #ifdef ENABLE_EXTI1
        HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(EXTI1_IRQn);
    #endif

    #ifdef ENABLE_EXTI2
        HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(EXTI2_IRQn);
    #endif

    #ifdef ENABLE_EXTI3
        HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(EXTI3_IRQn);
    #endif

    #ifdef ENABLE_EXTI4
        HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(EXTI4_IRQn);
    #endif

    #ifdef ENABLE_EXTI9_5
        HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
    #endif

    #ifdef ENABLE_EXTI15_10
        HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
    #endif
}

void exti_init_early(void) {
    stm32_exti_init_early();
}

static void stm32_exti_init(void)
{
    // nothing to do here
}

void exti_init(void) {
    stm32_exti_init();
}
