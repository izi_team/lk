/*
 * Copyright (c) 2020 IZITRON (Mihail CHERCIU)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <lk/trace.h>
#include <kernel/event.h>
#include <platform/adc.h>
#include <platform/platform_cm.h>

#define LOCAL_TRACE 1

struct adc_instance {
    ADC_HandleTypeDef handle_adc;
    DMA_HandleTypeDef handle_dma;
    event_t event;
    uint16_t buffer[ADC_CBUF_SIZE];
};

static struct adc_instance adc;

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
    arm_cm_irq_entry();
    event_signal(&adc.event, false);
    arm_cm_irq_exit(true);
}

void stm32_ADC_IRQ(void) {
    HAL_ADC_IRQHandler(&adc.handle_adc);
}

void stm32_DMA2_Stream0_IRQ(void) {
    HAL_DMA_IRQHandler(&adc.handle_dma);
}

/* Called by HAL_ADC_Init() */
void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc)
{
    switch ((uintptr_t)hadc->Instance) {
        case (uintptr_t)ADC1:
            __HAL_RCC_ADC1_CLK_ENABLE();

            adc.handle_dma.Instance = DMA2_Stream0;
            adc.handle_dma.Init.Channel = DMA_CHANNEL_0;
            adc.handle_dma.Init.Direction = DMA_PERIPH_TO_MEMORY;
            adc.handle_dma.Init.PeriphInc = DMA_PINC_DISABLE;
            adc.handle_dma.Init.MemInc = DMA_MINC_ENABLE;
            adc.handle_dma.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
            adc.handle_dma.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
            adc.handle_dma.Init.Mode = DMA_CIRCULAR;
            adc.handle_dma.Init.Priority = DMA_PRIORITY_LOW;
            adc.handle_dma.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
            if (HAL_DMA_Init(&adc.handle_dma) != HAL_OK)
            {
                Error_Handler();
            }

            __HAL_LINKDMA(hadc,DMA_Handle,adc.handle_dma);

            HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(ADC_IRQn);
            break;
        default:
            panic("unimplemented adc\n");
    }
}

static void stm32_adc_init_early(struct adc_instance *adc_ins, ADC_TypeDef *adc_nr)
{
    adc_ins->handle_adc.Instance = adc_nr;
    adc_ins->handle_adc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
    adc_ins->handle_adc.Init.Resolution = ADC_RESOLUTION_12B;
    adc_ins->handle_adc.Init.ScanConvMode = ENABLE;
    adc_ins->handle_adc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    adc_ins->handle_adc.Init.ContinuousConvMode = ENABLE;
    adc_ins->handle_adc.Init.DiscontinuousConvMode = DISABLE;
    adc_ins->handle_adc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
    adc_ins->handle_adc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
    adc_ins->handle_adc.Init.DMAContinuousRequests = ENABLE;
    adc_ins->handle_adc.Init.NbrOfConversion = 3;
    adc_ins->handle_adc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
    if (HAL_ADC_Init(&adc_ins->handle_adc) != HAL_OK)
    {
        Error_Handler();
    }
}

void adc_init_early(void) {
    stm32_adc_init_early(&adc, ADC1);
}

static void stm32_adc_init(struct adc_instance *adc_ins, uint32_t channel, uint8_t rank)
{
    ADC_ChannelConfTypeDef sConfig = {0};
    ADC_HandleTypeDef *hadc = &adc_ins->handle_adc;

    sConfig.Channel = channel;
    sConfig.Rank = rank;
    sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
    if (HAL_ADC_ConfigChannel(hadc, &sConfig) != HAL_OK)
    {
        Error_Handler();
    }
}

void adc_init(void) {
    event_init(&adc.event, false, EVENT_FLAG_AUTOUNSIGNAL);

#ifdef ENABLE_ADC_3
    stm32_adc_init(&adc, ADC_CHANNEL_3, 1);
#endif

#ifdef ENABLE_ADC_6
    stm32_adc_init(&adc, ADC_CHANNEL_6, 2);
#endif

#ifdef ENABLE_ADC_VREFINT
    stm32_adc_init(&adc, ADC_CHANNEL_VREFINT, 3);
#endif

    adc_start();
}

void adc_start(void) {
    if (HAL_ADC_Start_DMA(&adc.handle_adc, (uint32_t*)adc.buffer, ADC_CBUF_SIZE) != HAL_OK)
    {
        Error_Handler();
    }
}

void adc_stop(void) {
    if (HAL_ADC_Stop_DMA(&adc.handle_adc) != HAL_OK)
    {
        Error_Handler();
    }
}

uint16_t get_adc_value(adc_channel_t adc_channel) {
    uint16_t adc_value = 0;
    uint8_t nr_of_adc = 0;
    uint8_t mapping_ch[MAX_ADC];

#ifdef ENABLE_ADC_3
    mapping_ch[ADC_3] = nr_of_adc;
    nr_of_adc++;
#endif
#ifdef ENABLE_ADC_6
    mapping_ch[ADC_6] = nr_of_adc;
    nr_of_adc++;
#endif
#ifdef ENABLE_ADC_VREFINT
    mapping_ch[ADC_VREFINT] = nr_of_adc;
    nr_of_adc++;
#endif

if (nr_of_adc) {
    event_wait(&adc.event);
    adc_value = adc.buffer[mapping_ch[adc_channel]];
}
else {
    TRACEF("WARNING no ADC Channel\n");
}

    return adc_value;
}

void get_adc_buffer(adc_channel_t adc_channel, uint16_t *buf, uint16_t length) {
    int i, pos;
    uint8_t nr_of_adc = 0;
    uint8_t mapping_ch[MAX_ADC];

#ifdef ENABLE_ADC_3
    mapping_ch[ADC_3] = nr_of_adc;
    nr_of_adc++;
#endif
#ifdef ENABLE_ADC_6
    mapping_ch[ADC_6] = nr_of_adc;
    nr_of_adc++;
#endif
#ifdef ENABLE_ADC_VREFINT
    mapping_ch[ADC_VREFINT] = nr_of_adc;
    nr_of_adc++;
#endif

    if (nr_of_adc == 1) {
        if (length > ADC_CBUF_SIZE) {
            TRACEF("WARNING length too big, max: %d\n", ADC_CBUF_SIZE);
            length = ADC_CBUF_SIZE;
        }

        event_wait(&adc.event);

        for (i=0; i<length; i++) {
            buf[i] = adc.buffer[i];
        }
    }
    else if (nr_of_adc > 1) {
        if (length > ADC_CBUF_SIZE/nr_of_adc) {
            TRACEF("WARNING length too big, max: %d\n", ADC_CBUF_SIZE/nr_of_adc);
            length = ADC_CBUF_SIZE/nr_of_adc;
        }

        event_wait(&adc.event);

        /* pos is the position in queue adc_buffer */
        pos = mapping_ch[adc_channel];

        for (i=0; i<length; i++) {
            buf[i] = adc.buffer[(i*nr_of_adc) + pos];
        }
    }
    else {
        TRACEF("WARNING no ADC Channel\n");
    }
}
