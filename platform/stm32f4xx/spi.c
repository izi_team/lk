/*
 * Copyright (c) 2016 Erik Gilling
 * Copyright (c) 2020 IZITRON (Mihail CHERCIU)
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT
 */

#include <lk/err.h>
#include <kernel/mutex.h>
#include <platform/spi.h>
#include <platform/platform_cm.h>

#define NUM_SPI 3

struct spi_instance {
    SPI_HandleTypeDef handle;
    mutex_t lock;
};

#if ENABLE_SPI2
static struct spi_instance spi2;
#endif

#if ENABLE_SPI3
static struct spi_instance spi3;
#endif

static struct spi_instance *const spi[NUM_SPI + 1] = {
#if ENABLE_SPI2
    [2] = &spi2,
#endif
#if ENABLE_SPI3
    [3] = &spi3,
#endif
};

/* This function is called by HAL_SPI_Init() */
void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi) {
    switch ((uintptr_t)hspi->Instance) {
        case (uintptr_t)SPI2:
            __HAL_RCC_SPI2_CLK_ENABLE();
            HAL_NVIC_SetPriority(SPI2_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(SPI2_IRQn);
            break;
        case (uintptr_t)SPI3:
            __HAL_RCC_SPI3_CLK_ENABLE();
            HAL_NVIC_SetPriority(SPI3_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(SPI3_IRQn);
            break;
        default:
            panic("unimplemented spi\n");
    }
}

static void stm32_spi_early_init(struct spi_instance *spi_ins, SPI_TypeDef *hspi)
{
    spi_ins->handle.Instance = hspi;
    spi_ins->handle.Init.Mode = SPI_MODE_MASTER;
    spi_ins->handle.Init.Direction = SPI_DIRECTION_2LINES;
    spi_ins->handle.Init.DataSize = SPI_DATASIZE_8BIT;
    spi_ins->handle.Init.CLKPolarity = SPI_POLARITY_LOW;
    spi_ins->handle.Init.CLKPhase = SPI_PHASE_1EDGE;
    spi_ins->handle.Init.NSS = SPI_NSS_SOFT;
    spi_ins->handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
    spi_ins->handle.Init.FirstBit = SPI_FIRSTBIT_MSB;
    spi_ins->handle.Init.TIMode = SPI_TIMODE_DISABLE;
    spi_ins->handle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    spi_ins->handle.Init.CRCPolynomial = 10;

    if (HAL_SPI_Init(&spi_ins->handle) != HAL_OK)
    {
        Error_Handler();
    }

    mutex_init(&spi_ins->lock);
}

void spi_init_early(void) {
#ifdef ENABLE_SPI2
    stm32_spi_early_init(spi[2], SPI2);
#endif

#ifdef ENABLE_SPI3
    stm32_spi_early_init(spi[3], SPI3);
#endif
}

void spi_init(void) {

}

#ifdef ENABLE_SPI2
void stm32_SPI2_IRQ(void) {
    HAL_SPI_IRQHandler(&spi2.handle);
}
#endif

#ifdef ENABLE_SPI3
void stm32_SPI3_IRQ(void) {
    HAL_SPI_IRQHandler(&spi3.handle);
}
#endif

status_t spi_xfer(int bus, uint8_t *tx_buf, uint8_t *rx_buf, uint16_t len) {
    uint8_t ret = HAL_OK;

    struct spi_instance *spi_ins = spi[bus];
    if (bus < 0 || bus > NUM_SPI || !spi_ins)
        return ERR_BAD_HANDLE;

    mutex_acquire(&spi_ins->lock);
    ret = HAL_SPI_TransmitReceive(&spi_ins->handle, tx_buf, rx_buf, len, 1000);
    mutex_release(&spi_ins->lock);

    return ret;
}

status_t spi_transmit(int bus, uint8_t *tx_buf, uint16_t len) {
    uint8_t ret = HAL_OK;

    struct spi_instance *spi_ins = spi[bus];
    if (bus < 0 || bus > NUM_SPI || !spi_ins)
        return ERR_BAD_HANDLE;

    mutex_acquire(&spi_ins->lock);
    ret = HAL_SPI_Transmit(&spi_ins->handle, tx_buf, len, 1000);
    mutex_release(&spi_ins->lock);

    return ret;
}

status_t spi_receive(int bus, uint8_t *rx_buf, uint16_t len) {
    uint8_t ret = HAL_OK;

    struct spi_instance *spi_ins = spi[bus];
    if (bus < 0 || bus > NUM_SPI || !spi_ins)
        return ERR_BAD_HANDLE;

    mutex_acquire(&spi_ins->lock);
    ret = HAL_SPI_Receive(&spi_ins->handle, rx_buf, len, 1000);
    mutex_release(&spi_ins->lock);

    return ret;
}
